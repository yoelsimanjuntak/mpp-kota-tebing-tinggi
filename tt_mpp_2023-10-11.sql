# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: tt_mpp
# Generation Time: 2023-10-10 20:31:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _faqs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_faqs`;

CREATE TABLE `_faqs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Question` text,
  `Answer` text,
  `Name` varchar(200) DEFAULT '',
  `Timestamp` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_faqs` WRITE;
/*!40000 ALTER TABLE `_faqs` DISABLE KEYS */;

INSERT INTO `_faqs` (`Uniq`, `Question`, `Answer`, `Name`, `Timestamp`)
VALUES
	(2,'Apakah pendaftaran izin dapat dilakukan secara online?','Ya, anda dapat melakukan pendaftaran secara online melalui Aplikasi SiCantik untuk lebih jelas anda dapat mengunduh panduan aplikasi sicantik melalui link ini.','','2023-05-01 12:00:00'),
	(3,'Bagaimana pemohon mengetahui status proses perizinan yang dimohonkan?','Anda dapat melakukan tracking permohonan izin melalui akun yang telah didaftarkan melalui Aplikasi SiCantik Cloud atau anda juga dapat melakukan tracking permohonan melalui Tracking Permohonan dengan menginput nomor pendaftaran anda.','','2023-05-01 12:00:00'),
	(4,'Bagaimana melakukan pendaftaran perizinan berusaha?','Anda dapat melakukan pendaftaran melalui OSS RBA dengan terlebih dahulu mengetahui kode KBLI kegiatan usaha anda.','','2023-05-01 12:00:00'),
	(5,'Bagaimana mengetahui kode KBLI kegiatan usaha?','Anda dapat mengakses Informasi Kode KBLI melalui link https://oss.go.id/informasi/kbli-berbasis-risiko.','','2023-05-01 12:00:00');

/*!40000 ALTER TABLE `_faqs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _homepage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_homepage`;

CREATE TABLE `_homepage` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ContentID` varchar(20) DEFAULT NULL,
  `ContentTitle` varchar(200) DEFAULT NULL,
  `ContentType` varchar(50) NOT NULL,
  `ContentDesc1` text,
  `ContentDesc2` text,
  `ContentDesc3` text,
  `ContentDesc4` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_homepage` WRITE;
/*!40000 ALTER TABLE `_homepage` DISABLE KEYS */;

INSERT INTO `_homepage` (`Uniq`, `ContentID`, `ContentTitle`, `ContentType`, `ContentDesc1`, `ContentDesc2`, `ContentDesc3`, `ContentDesc4`)
VALUES
	(1,'TxtWelcome1','Pengantar','TEXT','<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','k_dinkes2.png',NULL,NULL),
	(2,'TxtWelcome2','Pengantar','TEXT','<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','k_dinkes2.png',NULL,NULL),
	(41,'NumProfile','NUMBER 1','NUMBER','Angka 1',NULL,'1','Satuan'),
	(42,'NumProfile','NUMBER 2','NUMBER','Angka 2',NULL,'2','Satuan'),
	(43,'NumProfile','NUMBER 3','NUMBER','Angka 3',NULL,'3','Satuan'),
	(44,'NumProfile','NUMBER 4','NUMBER','Angka 4',NULL,'4','Satuan'),
	(48,'TxtPopup1','Pengumuman','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(49,'TxtPopup2','FORKOPIMDA','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(50,'TxtPopup2','FKUB','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(51,'TxtPopup2','FPK','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(52,'TxtPopup2','FKDM','TEXT','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n',NULL,NULL,NULL),
	(55,'Carousel','Carousel 1','IMG',NULL,NULL,NULL,NULL),
	(56,'Carousel','Carousel 2','IMG',NULL,NULL,NULL,NULL),
	(57,'Carousel','Carousel 3','IMG',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_homepage` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_logs`;

CREATE TABLE `_logs` (
  `Uniq` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Timestamp` datetime DEFAULT NULL,
  `URL` text,
  `ClientInfo` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_logs` WRITE;
/*!40000 ALTER TABLE `_logs` DISABLE KEYS */;

INSERT INTO `_logs` (`Uniq`, `Timestamp`, `URL`, `ClientInfo`)
VALUES
	(1,'2023-10-10 21:54:20','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(2,'2023-10-10 22:39:17','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(3,'2023-10-10 22:42:08','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(4,'2023-10-10 22:43:22','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(5,'2023-10-10 22:44:31','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(6,'2023-10-10 22:44:40','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(7,'2023-10-10 22:44:46','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(8,'2023-10-10 22:44:49','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(9,'2023-10-10 22:45:07','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(10,'2023-10-10 22:45:18','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(11,'2023-10-10 22:45:23','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(12,'2023-10-10 22:46:21','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(13,'2023-10-10 22:46:33','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(14,'2023-10-10 22:47:17','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(15,'2023-10-10 22:47:52','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(16,'2023-10-10 22:49:56','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(17,'2023-10-10 22:51:43','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(18,'2023-10-10 22:53:57','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(19,'2023-10-10 22:59:01','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(20,'2023-10-10 23:01:28','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(21,'2023-10-10 23:04:09','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(22,'2023-10-10 23:04:48','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(23,'2023-10-10 23:05:05','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(24,'2023-10-10 23:06:12','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(25,'2023-10-10 23:06:20','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(26,'2023-10-10 23:06:32','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(27,'2023-10-10 23:07:12','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(28,'2023-10-10 23:07:39','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(29,'2023-10-10 23:08:11','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(30,'2023-10-10 23:09:22','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(31,'2023-10-10 23:09:52','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(32,'2023-10-10 23:10:08','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(33,'2023-10-10 23:10:14','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(34,'2023-10-10 23:10:39','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(35,'2023-10-10 23:11:51','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(36,'2023-10-10 23:13:36','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(37,'2023-10-10 23:15:10','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(38,'2023-10-10 23:17:03','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(39,'2023-10-10 23:18:33','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(40,'2023-10-10 23:19:25','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(41,'2023-10-10 23:19:36','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(42,'2023-10-10 23:21:41','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(43,'2023-10-10 23:21:52','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(44,'2023-10-10 23:22:32','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(45,'2023-10-10 23:23:21','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(46,'2023-10-10 23:23:59','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(47,'2023-10-10 23:24:18','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(48,'2023-10-10 23:25:17','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(49,'2023-10-10 23:25:50','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(50,'2023-10-10 23:27:03','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(51,'2023-10-10 23:27:13','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(52,'2023-10-10 23:27:21','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(53,'2023-10-10 23:28:34','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(54,'2023-10-10 23:28:54','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(55,'2023-10-10 23:29:05','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(56,'2023-10-10 23:29:17','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(57,'2023-10-10 23:29:56','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(58,'2023-10-10 23:30:16','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(59,'2023-10-10 23:30:35','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(60,'2023-10-10 23:30:42','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(61,'2023-10-10 23:31:13','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(62,'2023-10-10 23:31:40','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(63,'2023-10-10 23:31:49','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(64,'2023-10-10 23:31:58','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(65,'2023-10-10 23:32:17','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(66,'2023-10-10 23:32:37','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(67,'2023-10-10 23:33:17','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(68,'2023-10-10 23:33:33','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(69,'2023-10-10 23:33:48','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(70,'2023-10-10 23:34:05','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(71,'2023-10-10 23:34:24','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(72,'2023-10-10 23:34:58','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(73,'2023-10-10 23:35:12','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(74,'2023-10-10 23:35:24','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(75,'2023-10-10 23:35:37','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(76,'2023-10-10 23:36:46','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(77,'2023-10-10 23:39:00','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(78,'2023-10-10 23:39:50','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(79,'2023-10-10 23:44:10','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(80,'2023-10-10 23:44:49','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(81,'2023-10-10 23:45:05','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(82,'2023-10-10 23:45:15','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(83,'2023-10-10 23:45:25','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(84,'2023-10-10 23:45:30','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(85,'2023-10-10 23:46:30','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(86,'2023-10-10 23:46:43','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(87,'2023-10-10 23:47:46','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(88,'2023-10-10 23:48:13','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(89,'2023-10-10 23:48:33','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(90,'2023-10-10 23:48:59','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(91,'2023-10-10 23:49:14','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(92,'2023-10-10 23:49:30','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(93,'2023-10-10 23:49:58','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(94,'2023-10-10 23:50:28','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(95,'2023-10-10 23:53:45','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(96,'2023-10-10 23:54:09','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(97,'2023-10-10 23:54:37','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(98,'2023-10-10 23:55:04','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(99,'2023-10-10 23:55:19','http://localhost/tt-mpp/','::1 Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1'),
	(100,'2023-10-10 23:55:33','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(101,'2023-10-10 23:55:45','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(102,'2023-10-10 23:55:49','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(103,'2023-10-10 23:55:58','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(104,'2023-10-10 23:57:24','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(105,'2023-10-10 23:57:43','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(106,'2023-10-10 23:58:33','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(107,'2023-10-10 23:59:35','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(108,'2023-10-11 00:14:43','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(109,'2023-10-11 00:15:49','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(110,'2023-10-11 00:18:03','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(111,'2023-10-11 00:18:35','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(112,'2023-10-11 00:20:00','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(113,'2023-10-11 00:20:25','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(114,'2023-10-11 00:20:35','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(115,'2023-10-11 00:21:30','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(116,'2023-10-11 00:23:01','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(117,'2023-10-11 00:24:22','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(118,'2023-10-11 00:24:38','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(119,'2023-10-11 00:25:01','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(120,'2023-10-11 00:25:25','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(121,'2023-10-11 00:25:50','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(122,'2023-10-11 00:25:59','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(123,'2023-10-11 00:27:16','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(124,'2023-10-11 00:28:58','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(125,'2023-10-11 00:30:01','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(126,'2023-10-11 00:31:12','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(127,'2023-10-11 00:34:14','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(128,'2023-10-11 00:34:35','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(129,'2023-10-11 00:35:52','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(130,'2023-10-11 00:36:11','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(131,'2023-10-11 00:36:37','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(132,'2023-10-11 00:40:11','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(133,'2023-10-11 00:41:46','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(134,'2023-10-11 00:42:45','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(135,'2023-10-11 00:46:13','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(136,'2023-10-11 00:46:57','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(137,'2023-10-11 00:47:15','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(138,'2023-10-11 00:56:35','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(139,'2023-10-11 00:56:50','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(140,'2023-10-11 01:29:45','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(141,'2023-10-11 01:29:54','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(142,'2023-10-11 01:30:47','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(143,'2023-10-11 01:31:01','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(144,'2023-10-11 01:31:30','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(145,'2023-10-11 01:34:09','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(146,'2023-10-11 01:34:36','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(147,'2023-10-11 01:36:57','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(148,'2023-10-11 01:38:38','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(149,'2023-10-11 01:38:51','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(150,'2023-10-11 01:39:12','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(151,'2023-10-11 01:39:37','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(152,'2023-10-11 01:39:52','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(153,'2023-10-11 01:41:11','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(154,'2023-10-11 01:41:40','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(155,'2023-10-11 01:41:47','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(156,'2023-10-11 01:42:08','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(157,'2023-10-11 01:42:14','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(158,'2023-10-11 01:42:19','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(159,'2023-10-11 01:43:14','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(160,'2023-10-11 01:45:30','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(161,'2023-10-11 01:46:37','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(162,'2023-10-11 01:47:18','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(163,'2023-10-11 01:51:05','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(164,'2023-10-11 01:52:43','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(165,'2023-10-11 01:56:25','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(166,'2023-10-11 01:56:31','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(167,'2023-10-11 02:00:46','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(168,'2023-10-11 02:05:43','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(169,'2023-10-11 02:05:59','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(170,'2023-10-11 02:06:12','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(171,'2023-10-11 02:06:21','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(172,'2023-10-11 02:06:53','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(173,'2023-10-11 02:07:15','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(174,'2023-10-11 02:07:35','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(175,'2023-10-11 02:07:42','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(176,'2023-10-11 02:08:06','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(177,'2023-10-11 02:08:32','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(178,'2023-10-11 02:09:16','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(179,'2023-10-11 02:09:55','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(180,'2023-10-11 02:10:08','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(181,'2023-10-11 02:10:21','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(182,'2023-10-11 02:11:36','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(183,'2023-10-11 02:12:23','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(184,'2023-10-11 02:12:40','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(185,'2023-10-11 02:13:00','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(186,'2023-10-11 02:13:08','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(187,'2023-10-11 02:13:38','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(188,'2023-10-11 02:13:44','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(189,'2023-10-11 02:13:54','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(190,'2023-10-11 02:14:08','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(191,'2023-10-11 02:15:05','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(192,'2023-10-11 02:16:23','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(193,'2023-10-11 02:18:01','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(194,'2023-10-11 02:19:18','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(195,'2023-10-11 02:19:36','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(196,'2023-10-11 02:20:02','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(197,'2023-10-11 02:20:42','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(198,'2023-10-11 02:21:57','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(199,'2023-10-11 02:23:03','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(200,'2023-10-11 02:23:36','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(201,'2023-10-11 02:23:45','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(202,'2023-10-11 02:23:54','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(203,'2023-10-11 02:24:11','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(204,'2023-10-11 02:24:31','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(205,'2023-10-11 02:24:48','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(206,'2023-10-11 02:24:59','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(207,'2023-10-11 02:25:12','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(208,'2023-10-11 02:26:19','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(209,'2023-10-11 02:28:33','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(210,'2023-10-11 02:28:50','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(211,'2023-10-11 02:29:03','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(212,'2023-10-11 02:31:15','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(213,'2023-10-11 02:31:35','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(214,'2023-10-11 02:34:39','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(215,'2023-10-11 02:35:18','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(216,'2023-10-11 02:36:07','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(217,'2023-10-11 02:36:21','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(218,'2023-10-11 02:36:44','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(219,'2023-10-11 02:36:49','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(220,'2023-10-11 02:37:58','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(221,'2023-10-11 02:38:34','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(222,'2023-10-11 02:40:17','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(223,'2023-10-11 02:40:34','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(224,'2023-10-11 02:41:11','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(225,'2023-10-11 02:41:42','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(226,'2023-10-11 02:42:18','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(227,'2023-10-11 02:43:14','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(228,'2023-10-11 02:43:20','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(229,'2023-10-11 02:43:29','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(230,'2023-10-11 02:43:34','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(231,'2023-10-11 02:43:41','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(232,'2023-10-11 02:43:49','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(233,'2023-10-11 02:43:55','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(234,'2023-10-11 02:43:57','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(235,'2023-10-11 02:43:58','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(236,'2023-10-11 02:43:59','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(237,'2023-10-11 02:43:59','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(238,'2023-10-11 02:44:01','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(239,'2023-10-11 02:44:07','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(240,'2023-10-11 02:44:10','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(241,'2023-10-11 02:44:42','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(242,'2023-10-11 02:44:44','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(243,'2023-10-11 02:45:11','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(244,'2023-10-11 02:45:50','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(245,'2023-10-11 02:45:58','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(246,'2023-10-11 02:46:16','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(247,'2023-10-11 02:46:23','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(248,'2023-10-11 02:46:33','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(249,'2023-10-11 02:46:40','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(250,'2023-10-11 02:46:51','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(251,'2023-10-11 02:47:00','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(252,'2023-10-11 02:47:27','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(253,'2023-10-11 02:47:39','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(254,'2023-10-11 02:47:48','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(255,'2023-10-11 02:48:06','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(256,'2023-10-11 02:48:27','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(257,'2023-10-11 02:48:32','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(258,'2023-10-11 02:48:43','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(259,'2023-10-11 02:48:55','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(260,'2023-10-11 02:49:03','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(261,'2023-10-11 02:49:16','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(262,'2023-10-11 02:49:27','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(263,'2023-10-11 02:49:34','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(264,'2023-10-11 03:01:53','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(265,'2023-10-11 03:02:10','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(266,'2023-10-11 03:03:05','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(267,'2023-10-11 03:03:48','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(268,'2023-10-11 03:04:42','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(269,'2023-10-11 03:10:09','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(270,'2023-10-11 03:24:13','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36'),
	(271,'2023-10-11 03:28:25','http://localhost/tt-mpp/','::1 Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36');

/*!40000 ALTER TABLE `_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(2,'Berita','Berita & artikel terkini seputar Badan Kesbangpol',1,0,0),
	(4,'SKM','Survei Kepuasan Masyarakat',1,0,0),
	(3,'Galeri','Dokumentasi Kegiatan',1,0,0),
	(5,'Lainnya',NULL,1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostUnitID` int(11) DEFAULT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;



# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL,
  `RoleName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator'),
	(3,'Publik');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `SettingID` int(10) unsigned NOT NULL,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`Uniq`, `SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,1,'SETTING_WEB_NAME','SETTING_WEB_NAME','MPP TEBING TINGGI'),
	(2,2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Portal Mal Pelayanan Publik Digital'),
	(3,3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,4,'SETTING_ORG_NAME','SETTING_ORG_NAME','MAL PELAYANAN PUBLIK'),
	(5,5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','kesbangpol.tebingtinggi@gmail.com'),
	(11,11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(14,14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','main.gif'),
	(15,15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.01'),
	(16,16,'SETTING_ORG_REGION','SETTING_ORG_REGION','PEMERINTAH KOTA TEBING TINGGI'),
	(18,18,'SETTING_LINK_INSTAGRAM','SETTING_LINK_INSTAGRAM','-'),
	(19,19,'SETTING_LINK_FACEBOOK','SETTING_LINK_FACEBOOK',''),
	(20,20,'SETTING_LINK_YOUTUBE','SETTING_LINK_YOUTUBE',''),
	(21,21,'SETTING_LINK_GOOGLEMAP','SETTING_LINK_GOOGLEMAP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.017141346365!2d99.15632087461456!3d3.3459148519123345!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3031616f8735cfdd%3A0xe300a1c33a93cf14!2sMAL%20Pelayanan%20Publik%20Tebing%20Tinggi!5e0!3m2!1sen!2sid!4v1696949752346!5m2!1sen!2sid\" width=\"100%\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>'),
	(22,22,'SETTING_LINK_WHATSAPP','SETTING_LINK_WHATSAPP','-'),
	(23,23,'SETTING_LINK_INSTAGRAM_ACC','SETTING_LINK_INSTAGRAM_ACC','-');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNo` varchar(50) DEFAULT NULL,
  `ImgFile` varchar(250) DEFAULT NULL,
  `RegDate` date DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`Uniq`, `UserName`, `Email`, `Name`, `IdentityNo`, `BirthDate`, `Gender`, `Address`, `PhoneNo`, `ImgFile`, `RegDate`)
VALUES
	(1,'admin','administrator@eperizinan.tebingtinggikota.go.id','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `Uniq` bigint(20) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`Uniq`, `UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	(1,'admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2023-05-09 08:15:14','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table morganisasi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `morganisasi`;

CREATE TABLE `morganisasi` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `OrgNama` varchar(200) DEFAULT NULL,
  `OrgAlamat` text,
  `OrgPeriodeFrom` int(11) DEFAULT NULL,
  `OrgPeriodeTo` int(11) DEFAULT NULL,
  `OrgKategori` varchar(50) DEFAULT NULL,
  `OrgKetua` varchar(50) DEFAULT NULL,
  `OrgSekretaris` varchar(50) DEFAULT NULL,
  `OrgBendahara` varchar(50) DEFAULT NULL,
  `OrgNoSK1` varchar(200) DEFAULT NULL COMMENT 'SK Kementerian / Pusat',
  `OrgNoSK2` varchar(200) DEFAULT NULL COMMENT 'SK Pemerintah Daerah',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `morganisasi` WRITE;
/*!40000 ALTER TABLE `morganisasi` DISABLE KEYS */;

INSERT INTO `morganisasi` (`Uniq`, `OrgNama`, `OrgAlamat`, `OrgPeriodeFrom`, `OrgPeriodeTo`, `OrgKategori`, `OrgKetua`, `OrgSekretaris`, `OrgBendahara`, `OrgNoSK1`, `OrgNoSK2`)
VALUES
	(3,'ACEH SEPAKAT KOTA TEBING TINGGI','JL. SUTOYO NO. 14',2004,2009,'ORMAS KEGIATAN','-','-','-','-','-'),
	(4,'AISYIYAH','JL. S.M. RAJA NO. 47',2015,2022,'ORMAS WANITA','-','-','-','AHU-88AH.OI.07 TAHUN 2010','-'),
	(5,'AL-ITTIHADIYAH KOTA TEBING TINGGI','JLN. JENDRAL SUDIRMAN N0 A-6 ( SUDIRMAN BUSINES CENTER) LANTAI 2, KEL. SRI PADANG',2016,2021,'ORMAS AGAMA','-','-','-','83/SK.DPW-AISUMUT/X5/2016','-'),
	(6,'ALIANSI PEMERHATI KERUGIAN NEGARA','JL. BAJA KP.MARBUN',2008,2011,'LSM','-','-','-','-','-'),
	(7,'ANGKATAN MUDA KA\'BAH (AMK)','JL. BAWANG MERAH NO 1',2007,2012,'ORMAS PEMUDA','-','-','-','-','-'),
	(8,'ANGKATAN MUDA MELAYU INDONESIA (AMMI)','JL. BHAKTI NO 10',2009,2013,'ORMAS PEMUDA','-','-','-','-','-'),
	(9,'ANGKATAN MUDA PEMBAHARUAN INDONESIA (AMPI)','JL. TERI NO. 8',2010,2015,'ORMAS PEMUDA','-','-','-','-','-'),
	(10,'ANGKATAN MUDA SATKAR ULAMA INDONESIA (AMSI)','GG.RESMI NO 3',2011,2015,'ORMAS PEMUDA','-','-','-','-','-'),
	(11,'ANGKATAN MUDA SILIWANGI (AMS) KOTA TEBING TINGGI','JL SEI BAHILANG NO 250',2012,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(12,'ASOSIASI ANGKUTAN UMUM BECA BERMOTOR (AAUBB)','JL. THAMRIN NO 132',2006,2011,'ORMAS PROFESI','-','-','-','-','-'),
	(13,'ASOSIASI KONSULTAN PEMBANGUNAN DAN PEMUKIMAN INDONESIA','JL. G.SUBROTO NO 14',2004,2007,'ORMAS PROFESI','-','-','-','-','-'),
	(14,'ASOSIASI KONTRAKTOR LISTRIK INDONESIA (AKLI)','JL. SUDIRMAN NO 252',1994,1999,'ORMAS PROFESI','-','-','-','-','-'),
	(15,'ASOSIASI MANAGER SECURITY INDONESIA (AMSI)','JL. G.LEUSER G-28',2006,2011,'ORMAS PROFESI','-','-','-','-','-'),
	(16,'ASOSIASI PEDAGANG KELILING (ASPEK)','JL. KF.TANDEAN N0 189',2010,2015,'ORMAS PROFESI','-','-','-','-','-'),
	(17,'ASOSIASI PEDAGANG PAKAIAN BEKAS','JL. JAHE NO. 23 B',2003,2008,'ORMAS PROFESI','-','-','-','-','-'),
	(18,'ASOSIASI PEDAGANG PASAR GAMBIR (APPG)','JL. ISKANDAR MUDA NO 24',2002,2007,'ORMAS PROFESI','-','-','-','-','-'),
	(19,'ASOSIASI PENAMBANG RAKYAT INDONESIA (APRI)','GRIYA PRIMA BP. 7',2016,2021,'ORMAS PROFESI','-','-','-','-','-'),
	(20,'ASOSIASI PENGUSAHA ELPIJI KOTA TEBING TINGGI','JL. SUDIRMAN NO 399',2011,2014,'ORMAS PROFESI','-','-','-','-','-'),
	(21,'ASOSIASI PERUSAHAAN PENGUSAHA KONTRAKTOR KONTRUKSI INDONESIA (APKAINDO)','JL. KF.TANDEAN NO 151',2009,2014,'ORMAS PROFESI','-','-','-','-','-'),
	(22,'ASOSIASI PETERNAKAN INDONESIA (ASPETER INDONESIA)','JL. A. RAHIM LUBIS, GANG. KELUARGA NO. 24',2016,2021,'ORMAS KEGIATAN','-','-','-','-','-'),
	(23,'ASOSIASI PEWARTA PERS INDONESIA (APPI) KOTA TEBING TINGGI','Jl. Gn Leuser Blok Q No. 19, Kel. Tanjung Marulak Kec. Rambutan',2023,2028,'ORMAS KEGIATAN','-','-','-','AHU-0009165.AH.01.07 TAHUN 2022','-'),
	(24,'ASOSIASI REKANAN PENGADAAN BARANG DAN DISTRIBUTOR INDONESIA (ARDIN)','JL. SUBROTO NO 9',2001,2006,'ORMAS PROFESI','-','-','-','-','-'),
	(25,'ASOSIASI SWAUMILL INDUSTRI KAYU','JL. K.PANE NO 10',2007,2012,'ORMAS PROFESI','-','-','-','-','-'),
	(26,'ASOSIASI TENAGA AHLI JASA KONTRUKSI INDONESIA','JL. BAHBOLON NO 21',2004,2007,'ORMAS PROFESI','-','-','-','-','-'),
	(27,'ASOSIASI WARTAWAN DEMOKRASI INDONESIA','JL. GUNUNG LAUSER',2016,2021,'ORMAS PROFESI','-','-','-','-','-'),
	(28,'ASOSIASI WARTAWAN INDONESIA KOTA TEBING TINGGI','JL. D.TOBA  NO 48',2009,2013,'ORMAS PROFESI','-','-','-','-','-'),
	(29,'ASSOSIASI PENGUSAHA JASA KONTRUKSI INDONESIA (APK - INDO)','JL. L.SIKAPING NO 21',2004,2009,'ORMAS PROFESI','-','-','-','-','-'),
	(30,'BADAN BELA NEGARA PC.0204 TEBING TINGGI','Jl. T. Imam Bonjol Lk. 1 Kel. Tebing Tinggi Lama, Kec. Tebing Tinggi Kota-Tebing Tinggi',2022,2027,'ORMAS KEGIATAN','-','-','-','AHU-0072303.AH.01.07 TAHUN 2016','-'),
	(31,'BADAN KERJA SAMA SURAU (BKS)','JL. SUPRAPTO NO 48',1995,2000,'ORMAS AGAMA','-','-','-','-','-'),
	(32,'BADAN KERJASAMA ANTAR GEREJA (BKAG)','JL. RA. KARTINI NO.15 KOTA TEBING TINGGI',2017,2022,'ORMAS AGAMA','-','-','-','SKT KEMENDAGRI NO. 0215-00-00/151/XII/2017','-'),
	(33,'BADAN MUSYAWARAH PERGURUAN SWASTA (BMPS)','JL. SUNDORO NO 9',1996,2000,'ORMAS PROFESI','-','-','-','-','-'),
	(34,'BADAN PEMANTAU PELAKSANAAN PENDAPATAN ASLI DAERAH (BP2PAD)','JL. K.PANE NO 79',2003,2008,'LSM','-','-','-','-','-'),
	(35,'BADAN PEMBERDAYAAN PENGANGGURAN INDONESIA','JL. A.YANI NO 66',2003,2004,'LSM','-','-','-','-','-'),
	(36,'BADAN PENGURUS PUSAT ALIANSI MAHASISWA TEBING TINGGI','JLN. SISINGAMANGARAJA NO. 17 A KOTA TEBING TINGGI',2019,2020,'ORMAS PENDIDIKAN','-','-','-','AHU-0003864.AH.01.07 TAHUN 2020','-'),
	(37,'BADAN WAKAF INDONESIA PERWAKILAN KOTA TEBING TINGGI','JL. PENDIDIKAN NO. 4 KOTA T. TINGGI',2019,2022,'ORMAS AGAMA','-','-','-','-','-'),
	(38,'BAMAGNAS ( BADAN MUSYAWARAH ANTAR GEREJA NASIONAL','JL. Prof. YAMIN SH',2018,2023,'ORMAS AGAMA','-','-','-','011/SK-DPD/BAMAGNAS/X/2016','-'),
	(39,'BARISAN MUDA DEMOKRAT KOTA TEBING TINGGI','JL. K.PANE NO 51',2004,2009,'ORMAS KEGIATAN','-','-','-','-','-'),
	(40,'BARISAN MUDA PENEGAK AMANAT NASIONAL','JL. SUTOMO NO 6',2000,2005,'ORMAS PEMUDA','-','-','-','-','-'),
	(41,'BISON - RI','JL. SIMALUNGUN ',2014,2019,'LSM','-','-','-','-','-'),
	(42,'DEWAN HARIAN CABANG 45 (DHC 45)','JL. DR. SUTOMO NO 6',2010,2016,'ORMAS KEGIATAN','-','-','-','-','-'),
	(43,'DEWAN MASJID INDONESIA','Jl. Syech Beringin Lk III, Kel. Tebing Tinggi, Kec. Padang Hilir',2017,2022,'ORMAS KEGIATAN','-','-','-','AHU-000012659.01.07 TAHUN 2015','-'),
	(44,'DEWAN PENGURUS ALIANSI MASYARAKAT KOTA TEBING TINGGI','JL. SAKTI LUBIS NO.29 KEL. PASAR BARU KEC. TEBING TINGGI KOTA',2020,2024,'ORMAS KEGIATAN','-','-','-','AHU-0006214.AH.01.07 TAHUN 2020','-'),
	(45,'DEWAN PENGURUS CABANG FORUM KOMUNIKASI DINIYAH TAKMILIYAH (DPC-FKDT)','JLN. PROF. DR. HAMKA NO. 07 C KEL. DURIAN KEC.BAJENIS KOTA TEBING TINGGI ',2017,2022,'ORMAS PENDIDIKAN','-','-','-','AHU-0007528.AH.01.07 TAHUN 2016','-'),
	(46,'DEWAN PENGURUS CABANG KEHUTANAN, PERKAYUAN, PERTANIAN, SERIKAT BURUH SEJAHTERA INDONESIA','JL. GATOT SUBROTO NO 17',2003,2008,'ORMAS KEGIATAN','-','-','-','-','-'),
	(47,'DEWAN PENGURUS CABANG LSM WADAH GENERASI ANAK BANGSA (WGAB) TEBING TINGGI','Jln. Kesatria Lk.IV Kel. Satria Kec. Padang Hilir',2020,2025,'LSM','-','-','-','AHU-0074214.AH.01.07 TAHUN 2016','-'),
	(48,'DEWAN PENGURUS DAERAH BADAN KOMUNIKASI PEMUDA REMAJA MASJID INDONESIA (DPD BKPRMI) KOTA TEBING TINGGI','MASJID NURUL IMAN, JL. ASRAMA KODIM LK. VI, KEL. PERSIAKAN KOTA TEBING TINGGI',2021,2025,'ORMAS AGAMA','-','-','-','AHU-0032231.AH.01.07 TAHUN 2016','-'),
	(49,'DEWAN PENGURUS DAERAH LEMBAGA KONSERVASI LINGKUNGAN HIDUP KOTA TEBING TINGGI','JL. GUNUNG LEUSER BLOK C2 NO. 20 (PERUM GRIYA PRIMA BP7) LK. II KEL. TJG MARULAK HILIR KEC. RAMBUTAN',2023,2026,'LSM','-','-','-','AHU-0009226-AH.01.07 TAHUN 2018','-'),
	(50,'DEWAN PENGURUS DAERAH PERSATUAN PERAWAT NASIONAL INDONESIA KOTA TEBING TINGGI (PPNI)','JL. SIMALUNGUN NO. 7A. LT. 1 RS. UNPRI KOTA TEBING TINGGI',2017,2022,'ORMAS PROFESI','-','-','-','-','-'),
	(51,'DEWAN PENGURUS FORUM MAHASISWA ISLAM KOTA TEBING TINGGI','JL. THAMRIN GG TURI',2011,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(52,'DEWAN PENGURUS IKATAN PELAJAR KOTA TEBING TINGGI','JL. KARYA LK.III',2010,2015,'ORMAS PROFESI','-','-','-','-','-'),
	(53,'DEWAN PENGURUS IKATAN WANITA ISLAM KOTA TEBING TINGGI',' JL. KIPAS II',2010,2015,'ORMAS WANITA','-','-','-','-','-'),
	(54,'DEWAN PENGURUS IQRA CLUB KOTA TEBING TINGGI','JL. ABDUL RAHIM LBS',2013,2018,'ORMAS KEGIATAN','-','-','-','-','-'),
	(55,'DEWAN PENGURUS PEMUDA KREATIF KOTA TEBING TINGGI','JL. P.SUMATERA LK.III',2013,2018,'ORMAS KEGIATAN','-','-','-','-','-'),
	(56,'DEWAN PENGURUS PEMUDA REMAJA MASJID','JL. G. SORIK MERAPI',2013,2018,'ORMAS KEGIATAN','-','-','-','-','-'),
	(57,'DEWAN PENGURUS PONDOK MUSLIMAH KOTA TEBING TINGGI','JL. ABDUL HAMID LK IV',2013,2018,'ORMAS KEGIATAN','-','-','-','-','-'),
	(58,'DEWAN PENGURUS PONDOK SILATURAHMI ISLAM (FOSIL) KOTA TEBING TINGGI','JL. P.BURU LK.IV',2010,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(59,'DEWAN PIMPINAN CABANG FORUM BATAK INTELEKTUAL KOTA TEBING TINGGI','Jln. Yos Sudarso, Kel. Mekar Sentosa, Kec. Rambutan Kota Tebing Tinggi',2021,2026,'ORMAS PROFESI','-','-','-','AHU–0000905.AH.01.08 TAHUN 2021','-'),
	(60,'DEWAN PIMPINAN CABANG GERAKAN PEMANTAU KINERJA APARATUR NEGARA (GEMPUR) KOTA TEBING TINGGI ','Jln. Kesatria No.28, Kel. Satria, Kec. Padang Hilir, Kota Tebing Tinggi',2021,2026,'ORMAS KEGIATAN','-','-','-','AHU–0011582.AH.01.07 TAHUN 2018','-'),
	(61,'DEWAN PIMPINAN CABANG JARINGAN LASKAR NUSABANGSA','JL. GUNUNG  LAUSER LK. II KEL. TANJUNG MARULAK  KEC. RAMBUTAN',2017,2022,'LSM','-','-','-','-','-'),
	(62,'DEWAN PIMPINAN CABANG LEMBAGA ANTI NARKOTIKA (DPC - LAN)                                     ','JL. GUNUNG LEUSER NO.1 KEC. RAMBUTAN TEBING TINGGI',2019,2024,'ORMAS KEGIATAN','-','-','-',' AHU-0000826.AH.01.08 TAHUN 2019','-'),
	(63,'DEWAN PIMPINAN CABANG MACAN ASIA INDONESIA (DPC MAI) KOTA TEBING TINGGI                        ','JL. PULAU SUMBAWA NO. 9 KOTA TEBING TINGGI',2018,2023,'LSM','-','-','-','AHU-0000684.AH.01.08 TAHUN 2017','-'),
	(64,'DEWAN PIMPINAN CABANG PEMUDA BANGSO BATAK KOTA TEBING TINGGI','Jl.  Teri No.08, Kel. Badak Bejuang, Kec. Tebing Tinggi Kota ',2022,2025,'ORMAS PROFESI','-','-','-','AHU–0011790.AH.01.07 TAHUN 2021','-'),
	(65,'DEWAN PIMPINAN CABANG(DPC) LSM GERAKAN PEMANTAU KINERJA APARATUR NEGARA( GEMPUR)','Jln. Kesatria No.28, Kel. Satria',2021,2026,'LSM','-','-','-','AHU-0011601.AH.01. 07 TAHUN 2018','-'),
	(66,'DEWAN PIMPINAN DAERAH ASOSIASI PEDAGANG PASAR SELURUH INDONESIA','JL. GURAMI PSR. INPRES KEL. BADAK BEJUANG KEC. T.TINGGI KOTA',2019,2024,'ORMAS PROFESI','-','-','-','AHU-0045220.AH.01.07 TAHUN 2016','-'),
	(67,'DEWAN PIMPINAN DAERAH LUMBUNG INFORMASI RAKYAT (LIRA)','JL. GUNUNG MERAPI. KO. PEMKO BLOK. R. NO. 42 LK. IV, KEL. TANJUNG MARULAK, KEC. RAMBUTAN',2017,2022,'LSM','-','-','-','-','-'),
	(68,'DEWAN PIMPINAN KOTA MASYARAKAT PANCASILA INDONESIA (MPI) KOTA TEBING TINGGI','JL. A. YANI NO 40 C',2014,2019,'ORMAS PEMUDA','-','-','-','-','-'),
	(69,'DEWAN PIMPINAN NASIONAL LEMBAGA PENGAWASAN PELAKSANA PELANGGARAN HUKUM REPUBLIK INDONESIA (LP3H-RI)','JL. TAMAN BAHAGIA NO. 9 KEL. TANJUNG MARULAK HILIR KEC. RAMBUTAN',2018,2023,'LSM','-','-','-','-','-'),
	(70,'DEWAN PIMPINAN NASIONAL LEMBAGA PENGAWASAN PELAKSANA PELANGGARAN HUKUM (LP3H)','JL.TAMAN BAHAGIA NO.9 KEL MARULAK HILIR KEC. RAMBUTAN KOTA T. TINGGI',2020,2025,'ORMAS KEGIATAN','-','-','-','AHU-00012394.AH.01.08 TAHUN 2020','-'),
	(71,'DEWAN PIMPINAN PUSAT IKATAN KEBATINAN BUDI SUCI INDONESIA KOTA TEBING TINGGI','Jln. Deblot Sundoro No.84 A, Kel. Bagelen, Kota Tebing Tinggi',2021,2026,'ORMAS PROFESI','-','-','-','AHU–0010765.AH.01.07 TAHUN 2021','-'),
	(72,'DHARMA PERTIWI','ASRAMA KODIM 0204 DS',1996,2001,'ORMAS WANITA','-','-','-','-','-'),
	(73,'DHARMA WANITA','JL.DR.SUTOMO  NO 3',2010,2015,'ORMAS WANITA','-','-','-','-','-'),
	(74,'DIAN KEMALA','JL. ISKANDAR MUDA NO 1A',2002,2006,'ORMAS WANITA','-','-','-','-','-'),
	(75,'DPC GARDA BELA NEGARA NASIONAL (GBNN) KOTA T. TINGGI','Jln. Ir. H. Juanda No.07, Lik. 02 Kel. Karya Jaya,Kec. Rambutan, Kota Tebing Tinggi',2021,2024,'ORMAS KEGIATAN','-','-','-','AHU-0011142.AH.01.07 TAHUN 2019','-'),
	(76,'DPC HIMPUNAN AHLI RIAS PENGANTIN INDONESIA MELATI','Jl. Danau Toba Lk. II, Kel. Pelita, Kec. Bajenis, Kota Tebing Tinggi',2021,2026,'ORMAS PROFESI','-','-','-','AHU-0010246.AH.01.07 TAHUN 2018','-'),
	(77,'DPC LASKAR HARIMAU SUMATERA','JL. TUALANG NO. 250',2013,2018,'ORMAS KEGIATAN','-','-','-','-','-'),
	(78,'DPC PATRIOT NASIONAL (PATRON)','Jl. IMAM BONJOL NO 43',2012,2017,'ORMAS KEGIATAN','-','-','-','-','-'),
	(79,'DPC PEMERHATI KINERJA APARATUR NEGARA KOTA TEBING TINGGI (PERKARA)','JL. KARTINI NO 54 KEC. T.TINGGI KOTA KEL. T. TINGGI LAMA',2019,2022,'LSM','-','-','-','AHU-0080434.AH.01.07 TAHUN 2016','-'),
	(80,'DPC PEMUDA BATAK BERSATU (PBB) KOTA TEBING TINGGI','JL.AHMAD YANI LK. IX KEL. BANDAR SONO, KEC. PADANG HULU, KOTA TEBING TINGGI                             ',2021,2026,'ORMAS KEGIATAN','-','-','-','AHU-0010985.AH.01.07 TAHUN 2019','-'),
	(81,'DPC PENGUSAHA MIKRO KECIL DAN MENENGAH (PMKM) PRIMA INDONESIA                                                                                                         ','JL.SUDIRMAN NO. 337 LT.II (DEPAN RS. PAMELA) KEC. RAMBUTAN, KEL. SRI PADANG KOTA T. TINGGI',2020,2026,'ORMAS PROFESI','-','-','-','AHU-0003560.AH.01.07 TAHUN 2020','-'),
	(82,'DPC PERKUMPULAN KONTRAKTOR PROFESIONAL INDONESIA (PERKOPINDO)','JL. PATIMURA NO. 75 TEBING TINGGI',2018,2023,'ORMAS PROFESI','-','-','-','AHU-0050107.AH.01.07 TAHUN 2016','-'),
	(83,'DPC PERKUMPULAN PENYANDANG DISABILITAS INDONESIA (PPDI) KOTA TEBING TINGGI','JL. M. J. SUTOYO LK. III, Kel. RAMBUNG, Kec. T. TINGGI KOTA',2022,2025,'ORMAS KEGIATAN','-','-','-','AHU-0004211.AH.01.07 TAHUN 2017','-'),
	(84,'DPC SENTRAL ORGANISASI KARYAWAN SWADIRI INDONESIA (SOKSI)','JL. DEBLOD SUNDORO LK. I KEL. BAGELEN',2020,2025,'ORMAS KEGIATAN','-','-','-','AHU-0000901.AH.01.08  TAHUN 2020','-'),
	(85,'DPD LSM LEMBAGA PEMERHATI DESA DAN KELURAHAN','JALAN TUALANG NO.300 LK III KELURAHAN BAGELEN ',2017,2022,'LSM','-','-','-','-','-'),
	(86,'DPD LSM MASYARAKAT PEDULI PENDIDIKAN KOTA TEBING TINGGI','JALAN MAHKAMAH LK V KELURAHAN SEI PADANG',2017,2022,'LSM','-','-','-','-','-'),
	(87,'DPD ORMAS PERKUMPULAN PEDANG KEADILAN PERJUANGAN KOTA TEBING TINGGI','Jl. Bahbolon No. 30 Lk. 7 Kel. Durian, Kec. Bajenis Kota Tebing Tinggi',2022,2027,'ORMAS KEGIATAN','-','-','-','AHU-0011379.AH.01.07 TAHUN 2019','-'),
	(88,'DPD PEMUDA MERGA SILIMA KOTA TEBING TINGGI','JL. D.I Panjaitan Kel. Rambung, Kec. Tebing Tinggi Kota',2021,2026,'ORMAS KEGIATAN','-','-','-','AHU-0061026.AH.01.07 TAHUN 2016','-'),
	(89,'DPD PERKUMPULAN TENAGA AHLI PROFESIONAL INDONESIA (PERTAPIN)                                                                                                         ','JL. PATIMURA NO. 75 TEBING TINGGI',2018,2023,'ORMAS PROFESI','-','-','-','AHU-0050111.AH.01.07 TAHUN 2016','-'),
	(90,'DPD RAKYAT PERMATA NUSANTARA (DPD RPN)','Jln. Bahbolon No.30 Lk.VII, Kel. Durian, Kec. Bajenis Kota Tebing Tinggi',2021,2026,'ORMAS KEGIATAN','-','-','-','AHU-0012166.AH.01.07 TAHUN 2018','-'),
	(91,'FEDERASI SERIKAT PEKERJA ROKOK, TEMBAKAU, MAKANAN DAN MINUMAN ','JL.G.LAUSER B-5',2008,2013,'ORMAS PROFESI','-','-','-','-','-'),
	(92,'FORUM DA\'I DAN USTADZ MUDA (FODIUM) TEBING TINGGI','Jln. Tuanku Imam Bonjol No. 16 A Kel. Tambangan, Kec. P. Hilir, Kota Tebing Tinggi',2020,2023,'ORMAS AGAMA','-','-','-','AHU–0014484.AH.01.07 TAHUN 2018','-'),
	(93,'FORUM KOMUNIKASI ANTAR ETNIS (FKAE)','JL. GATOT SUBROTO NO 44',2003,2008,'ORMAS KEGIATAN','-','-','-','-','-'),
	(94,'FORUM KOMUNIKASI ANTAR SEKTE UMAT BUDHA','JL. SUDIRMAN 315',2003,2006,'ORMAS AGAMA','-','-','-','-','-'),
	(95,'FORUM KOMUNIKASI PERJUANGAN REFORMASI ','JL. SENANGIN NO 4A',2005,2007,'ORMAS KEGIATAN','-','-','-','-','-'),
	(96,'FORUM KOMUNIKASI USAHA KECIL MENENGAH','JL. ISKANDAR MUDA NO 24',2005,2010,'ORMAS PROFESI','-','-','-','-','-'),
	(97,'FORUM MAHASISWA TEBING TINGGI DELI SUMUT ','JL.LANGSAT NO 46',2003,2005,'ORMAS PROFESI','-','-','-','-','-'),
	(98,'FORUM PEDULI PEREMPUAN KOTA TEBING TINGGI','JL. GATOT SUBROTO LK. IV TEBING TINGGI',2014,2019,'ORMAS WANITA','-','-','-','-','-'),
	(99,'FORUM PERS MEDIA MINGGUAN INDONESIA (PD FPM2I) KOTA TEBING TINGGI','JL. IMAM BONJOL NO 17',2010,2013,'ORMAS PROFESI','-','-','-','-','-'),
	(100,'FRONT KOMUNITAS INDONESIA SATU (FKI-I)','JL. LAMA',2012,2017,'ORMAS KEGIATAN','-','-','-','-','-'),
	(101,'GABUNGAN KARYAWAN PEMBANGUNAN INDONESIA (GKPI)','JL. DR. SUTOMO NO 14',1995,2000,'ORMAS KEGIATAN','-','-','-','-','-'),
	(102,'GABUNGAN PELAKSANA KONSTRUKSI NASIONAL INDONESIA (GAPENSI)','JL. AHMAD YANI NO. 85',2014,2019,'ORMAS PROFESI','-','-','-','-','-'),
	(103,'GABUNGAN PENGUSAHA KONTRAKTOR AIR INDONESIA','JL. P.PEJUANG NO 8',2006,2011,'ORMAS PROFESI','-','-','-','-','-'),
	(104,'GENERASI MUDA FKPPI (GM FKPPI)','JL. PATTIMURA NO 73',2007,2012,'ORMAS PEMUDA','-','-','-','-','-'),
	(105,'GENERASI MUDA KOSGORO (GM KOSGORO)','JL. 13 DESEMBER 40',1996,2001,'ORMAS PEMUDA','-','-','-','-','-'),
	(106,'GENERASI MUDA PADANG PARIAMAN (GEMPAR)','JL. A.YANI NO 40G',2007,2012,'ORMAS KEGIATAN','-','-','-','-','-'),
	(107,'GENERASI MUDA PEMBANGUNAN INDONESIA (GMPI)','JL. YOS SUDARSO NO 80A',2011,2016,'ORMAS PEMUDA','-','-','-','-','-'),
	(108,'GENERASI MUDA TRIKORA (GM TRIKORA)','JL. ANTURMANGAN NO 6',1996,2001,'ORMAS PEMUDA','-','-','-','-','-'),
	(109,'GERAKAN PEMUDA ALWASLIYAH (GPA)','JL.13 DESEMBER NO 3',2008,2011,'ORMAS PEMUDA','-','-','-','-','-'),
	(110,'GERAKAN ANGKATAN MUDA KRISTEN INDONESIA (GAMKI)','JL. TERI NO 8',2012,2017,'ORMAS PEMUDA','-','-','-','-','-'),
	(111,'GERAKAN ANTI KORUPSI DAN PENYELAMATAN ASET NEGARA (GAKORPAN) TEBING TINGGI - SERGAI','Jl. Nangka No. 18 Lk. I Kel. Rambung, Kec. Tebing Tinggi',2022,2025,'LSM','-','-','-','SKT KEMENDAGRI NO. 01-00-00/113/III/2018','-'),
	(112,'GERAKAN ANTI NARKOBA KOTA TEBING TINGGI','JL. SUDIRMAN NO 319',2004,2009,'LSM','-','-','-','-','-'),
	(113,'GERAKAN KEADILAN','JL. DR. HAMKA NO. 4A             ',2010,2015,'LSM','-','-','-','-','-'),
	(114,'GERAKAN MASYARAKAT PELESTARI LINGKUNGAN HIDUP','JL. S.BABURA NO 40',2003,2008,'LSM','-','-','-','-','-'),
	(115,'GERAKAN MENYUARAKAN ASPIRASI RAKYAT INDONESIA (GEMARI)','GG. H.M.Y. SINAGA                  ',2011,2015,'LSM','-','-','-','-','-'),
	(116,'GERAKAN MUDA BANTENG PERJUANGAN (GARDA BANPER)','JL. KARTINI NO 49A',2000,2004,'ORMAS PEMUDA','-','-','-','-','-'),
	(117,'GERAKAN NASIONAL ANTI NARKOTIKA (GRANAT) DEWAN PIMPINAN CABANG TEBING TINGGI','JL. KOL. YOS SUDARSO NO. 50 TEBING TINGGI',2020,2025,'ORMAS KEGIATAN','-','-','-','SKT KEMENDAGRI NO. 01-00-00/066/D.IV/IX/2016','-'),
	(118,'GERAKAN PEDULI PERKOTAAN (GPP) KOTA TEBING TINGGI','JL.JATI NO.26 PERUM BAGELEN',2016,2021,'LSM','-','-','-','-','-'),
	(119,'GERAKAN PEMUDA DAN MAHASISWA KOTA TEBING TINGGI','JL. P.IRIAN NO 22',2011,2013,'ORMAS KEGIATAN','-','-','-','-','-'),
	(120,'GERAKAN PEMUDA ISLAM ( GPI ) ','JL. YOS SUDARSO NO 70',2002,2005,'ORMAS PEMUDA','-','-','-','-','-'),
	(121,'GERAKAN PEMUDA KEADILAN (GPK)','JL. SM.RAJA NO 10',2004,2009,'ORMAS PEMUDA','-','-','-','-','-'),
	(122,'GERAKAN RAKYAT BERANTAS KORUPSI (GEBRAK)','JL. AMD NO. 4',2007,2012,'LSM','-','-','-','-','-'),
	(123,'GERAKAN REFORMASI INDONESIA (GERINDO)','GG. MERANTI NO 9',2003,2006,'LSM','-','-','-','-','-'),
	(124,'HIMPUNAN KARYA PEDAGANG EKONOMI LEMAH','JL. P.PEJUANG NO.28',1998,2002,'ORMAS PROFESI','-','-','-','-','-'),
	(125,'HIMPUNAN KELUARGA BESAR MANDAILING (HIKMA)','JL. YOS SUDARSO NO. 26',2010,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(126,'HIMPUNAN KELUARGA BESAR SIMPANG DOLOK DAN SEKITARNYA (SIMDOTAR)','JL. Ir. JUANDA NO 21',2003,2006,'ORMAS KEGIATAN','-','-','-','-','-'),
	(127,'HIMPUNAN KELUARGA NIAS (HIKNI)','JL. LANGSAT NO 26',2010,2013,'ORMAS KEGIATAN','-','-','-','-','-'),
	(128,'HIMPUNAN MAHASISWA DAN PEMUDA SIMALUNGUN (HIMAPSI)','JL. P.SAMOSIR NO 19',2010,2015,'ORMAS PEMUDA','-','-','-','-','-'),
	(129,'HIMPUNAN PEDAGANG PUSAT PASAR GAMBIR','JL. P.PEJUANG NO.25',2005,2008,'ORMAS PROFESI','-','-','-','-','-'),
	(130,'HIMPUNAN PEMUDA INDONESIA','JL. SIMALUNGUN NO 22',2006,2011,'ORMAS PEMUDA','-','-','-','-','-'),
	(131,'HIMPUNAN PENGUSAHA MIKRO  DAN KECIL INDONESIA','JL. SUDIRMAN  50 B',2014,2019,'ORMAS KEGIATAN','-','-','-','-','-'),
	(132,'HIMPUNAN WANITA KARYA (HWK)','JL. GUNUNG LEUSER NO. 20 A',1993,1998,'ORMAS WANITA','-','-','-','-','-'),
	(133,'ICMI','JL. IMAM BONJOL',1995,2000,'ORMAS KEGIATAN','-','-','-','-','-'),
	(134,'IKATAN ALUMNI PMII KOTA TEBING TINGGI','JL. BAHBOLON NO 4',2009,2014,'ORMAS KEGIATAN','-','-','-','-','-'),
	(135,'IKATAN BIDAN INDONESIA (IBI)','JL.K. PANE  NO 226',2008,2013,'ORMAS PROFESI','-','-','-','-','-'),
	(136,'IKATAN GURU RAUDHATUL ATHFAL (IGRA)','JL. 13 DESEMBER',2016,2021,'ORMAS PROFESI','-','-','-','-','-'),
	(137,'IKATAN JAKA DARA (IKADARA)','JL. A.BILAL NO 25',2007,2012,'ORMAS KEGIATAN','-','-','-','-','-'),
	(138,'IKATAN KADER SAHABAT (IKASA)','JL. Ir. JUANDA NO 105',2003,2007,'ORMAS KEGIATAN','-','-','-','-','-'),
	(139,'IKATAN KELUARGA BESAR ARIF RAHMAN HAKIM','JL. BADAK NO 135F',1995,2000,'ORMAS PEMUDA','-','-','-','-','-'),
	(140,'IKATAN KELUARGA BESAR HASIBUAN ','JL. PENDIDIKAN NO. 4',1999,2004,'ORMAS KEGIATAN','-','-','-','-','-'),
	(141,'IKATAN PELAJAR MUHAMMADIYAH (IPM)','JL. SM.RAJA NO 47 LK 3 KEL. BANDARSONO KOTA TEBING TINGGI',2020,2022,'ORMAS PEMUDA','-','-','-','-','-'),
	(142,'IKATAN PELAJAR NAHDATUL ULAMA (IPNU)','JL. M. YAMIN SH',2005,2009,'ORMAS PEMUDA','-','-','-','-','-'),
	(143,'IKATAN PEMUDA KARYA (IPK)','JL. IMAM BONJOL NO 47',2009,2014,'ORMAS PEMUDA','-','-','-','-','-'),
	(144,'IKATAN PENGELOLA BKB DAN PENDIDIKAN PADU','JL. CIKRAMLAH',2005,2010,'ORMAS KEGIATAN','-','-','-','-','-'),
	(145,'IKATAN PERSAUDARAAN PEMUDA TARBIYAH ISLAMIYAH','JL. PEPAYA NO 17',2005,2010,'ORMAS PEMUDA','-','-','-','-','-'),
	(146,'IKATAN PUTRA NAHDATUL ULAMA','JL. IMAM BONJOL NO 16A',2002,2004,'ORMAS PEMUDA','-','-','-','-','-'),
	(147,'IKATAN PUTRA-PUTRI ALWASLIYAH (IPPA)','JL. 13 DESEMBER NO 3',2005,2009,'ORMAS PEMUDA','-','-','-','-','-'),
	(148,'IKATAN SARJANA ALWASHLIYAH ','Jl. Batubara Lk V, Kel. Satria, Kec. Padang Hilir',2021,2024,'ORMAS KEGIATAN','-','-','-','AHU-0002320.01.07 TAHUN 2018','-'),
	(149,'IKATAN TUNANETRA MUSLIM INDONESIA (ITMI)','KOMPLEK BALAI DEWA',2011,2016,'ORMAS PROFESI','-','-','-','-','-'),
	(150,'INDONESIA CORRUPTION WATCH (ICW)','JL. GATOT SUBROTO LK 2 NO. 28',2014,2019,'LSM','-','-','-','-','-'),
	(151,'INDONESIA GREEN SOCIAL YOUTH MOVEMENT (IGRESYM)','JL. DR. HAMKA LK. 02',2013,2018,'LSM','-','-','-','-','-'),
	(152,'JARINGAN PENDAMPING KEBIJAKAN PEMBANGUNAN','JL.KUTILANG PERUMAHAN BAJENIS INDAH LK. VI',2016,2021,'ORMAS KEGIATAN','-','-','-','AHU 00011682. AH. 01. 07 TAHUN 2015','-'),
	(153,'KAMAR DAGANG DAN INDUSTRI','JL. KF TANDEAN NO 28',2001,2006,'ORMAS KEGIATAN','-','-','-','-','-'),
	(154,'KARANG TARUNA INDONESIA','JL. IMAM BONJOL NO 5',2012,2017,'ORMAS KEGIATAN','-','-','-','-','-'),
	(155,'KELUARGA BESAR PADANG PARIAMAN','Jl. RAHIM LUBIS 6',2012,2016,'ORMAS KEGIATAN','-','-','-','-','-'),
	(156,'KELUARGA BESAR PUTRA PUTRI POLRI','JL. D. SUNDORO NO. 20',2011,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(157,'KELUARGA BESAR WIRAWATI CATUR PANCA WANITA PEJUANG 45 (KB WCP 45)','JL. VETERAN NO 38',2011,2013,'ORMAS WANITA','-','-','-','-','-'),
	(158,'KEMILAU CAHAYA BANGSA INDONESIA','JL. KELEPA SAWIT',2014,2019,'LSM','-','-','-','-','-'),
	(159,'KESATUAN AKSI PEMUDA PELAJAR INDONESIA KOTA TEBING TINGGI (FKB-KAPPI)','JL. VETERAN NO 38',2008,2012,'ORMAS PEMUDA','-','-','-','-','-'),
	(160,'KESATUAN ORGANISASI SERBA GUNA GOTONG ROYONG','JL. 13 DESEMBER',2002,2005,'ORMAS KEGIATAN','-','-','-','-','-'),
	(161,'KOETA OESANK WEIGHTLIFTING CLUB','JLN. GUNUNG AGUNG NO.9 KEL. TANJUNG MARULAK KEC. RAMBUTAN KOTA TEBING TINGGI',2020,2025,'ORMAS KEGIATAN','-','-','-','SKT KEMENDAGRI NO. 0215-00-00/124/VI/2020','-'),
	(162,'KOMISI PEMANTAU ASET DAN KEUANGAN NEGARA','JL. G. BAKTI, NO. 1A',2012,2017,'LSM','-','-','-','-','-'),
	(163,'KOMITE NASIONAL PEMUDA INDONESIA (KNPI) ','JL. TUANKU IMAM BONJOL, KEL. TEBING TINGGI LAMA, KEC. TEBING TINGGI KOTA',2020,2023,'ORMAS PEMUDA','-','-','-','AHU-0000037.AH.01.08 TAHUN 2019','-'),
	(164,'KOMUNITAS LINGKUNGAN INFOKOM (KLIK) KOTA TEBING TINGGI','JL. G.SIBAYAK NO. 72',2010,2013,'LSM','-','-','-','-','-'),
	(165,'KOMUNITAS PEDAGANG ANTAR KOTA (KOMPAK) KOTA TEBING TINGGI','KEL. MANDAILING LK. 4',2009,2014,'ORMAS PROFESI','-','-','-','-','-'),
	(166,'KOMUNITAS PELAPOR ANTI KORUPSI RI (LSM KOMPAK-RI)','JL.BADAK LK.I,KEL B. UTAMA',2014,2019,'LSM','-','-','-','-','-'),
	(167,'KOMUNITAS PEMBURU KORUPSI (KPK)','JL. B.BARISAN NO. 13',2009,2014,'LSM','-','-','-','-','-'),
	(168,'KOMUNITAS PEMUDA PEDULI SOSIAL DAN LINGKUNGAN HIDUP KOTA TEBING TINGGI','JL. KF.TANDEAN LK.1',2011,2013,'LSM','-','-','-','-','-'),
	(169,'KONFEDERASI SERIKAT PEKERJA SELURUH INDONESIA (KSPSI)','JL. G.LEUSER B-5',2008,2013,'ORMAS PROFESI','-','-','-','-','-'),
	(170,'KORPS ALUMNI HIMPUNAN MAHASISWA ISLAM (KAHMI) KOTA TEBING TINGGI','JL. GATOT SUBROTO LING. II KEL. PABATU KEC. PADANG HULU KOTA TEBING TINGGI',2021,2026,'ORMAS KEGIATAN','-','-','-','AHU-0000428.AH.01.08 TAHUN 2018','-'),
	(171,'KRIDA WANITA SWADIRI INDONESIA (KWSI)','JL. S.BAHILANG NO 11',2005,2010,'ORMAS WANITA','-','-','-','-','-'),
	(172,'LASKAR KEADILAN REPUBLIK INDONESIA','JL. PANJAITAN',2017,2022,'LSM','-','-','-','-','-'),
	(173,'LEGIUN VETERAN RI (LVRI)','JL. VETERAN NO 38',2010,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(174,'LEMBAGA ANTAR KEADILAN (LAKI)','JL. MAHONI NO. 272',2011,2016,'LSM','-','-','-','-','-'),
	(175,'LEMBAGA BANTUAN HUKUM POLDA SUMUT RESORT TEBING TINGGI','JL. GELATIK NO 1',2011,2012,'ORMAS PROFESI','-','-','-','-','-'),
	(176,'LEMBAGA BELA NEGARA RI KORPS BRIGADE KARYA DARMA PUTRA PEJUANG TRIKORA','JL. YOSUDARSO NO. 5B',2014,2019,'ORMAS KEGIATAN','-','-','-','-','-'),
	(177,'LEMBAGA DAKWAH ISLAM INDONESIA ( LDII )','JL. P.BELITUNG NO 16',2011,2016,'ORMAS AGAMA','-','-','-','-','-'),
	(178,'LEMBAGA INDEPENDEN PENYELAMAT  ASET DAERAH','BTN PURNAMA DELI NO.13',2008,2011,'LSM','-','-','-','-','-'),
	(179,'LEMBAGA INVESTIGASI TINDAK PIDANA KORUPSI  (LI-TPK) KOTA TEBING TINGGI','JL. KF.TANDEAN NO. 289',2010,2015,'LSM','-','-','-','-','-'),
	(180,'LEMBAGA KALI GRAFI AL HAFIZ KOTA TEBING TINGGI','JL. BERLIAN LK. I',2016,2021,'ORMAS KEGIATAN','-','-','-','-','-'),
	(181,'LEMBAGA KOMUNITAS INDEPENDENT SBY','JL. A. YANI NO.3',2004,2006,'LSM','-','-','-','-','-'),
	(182,'LEMBAGA KONSUMEN INDONESIA KOTA TEBING TINGGI','JL. IMAM BONJOL NO.17 B',2016,2021,'LSM','-','-','-','-','-'),
	(183,'LEMBAGA PEDULI ANTI NARKOBA DAN TAURAN','JL. BANDAR KAJUM NO. 1',2003,2008,'LSM','-','-','-','-','-'),
	(184,'LEMBAGA PEMANTAU KEUANGAN NEGARA',' JL. ABDI LK. IV',2014,2019,'LSM','-','-','-','-','-'),
	(185,'LEMBAGA PEMANTAU OTONOMI DAERAH (LPOD)','JL. BANI HASYIM NO.21',2005,2010,'LSM','-','-','-','-','-'),
	(186,'LEMBAGA PEMANTAUAN TINDAK PIDANA KORUPSI (LP TIPIKOR) NUSANTARA KOTA TEBING TINGGI','JL.LADA HITAM LK.04',2016,2021,'LSM','-','-','-','-','-'),
	(187,'LEMBAGA PEMBELA KEMERDEKAAN RAKYAT (PAKAR)','JL. KUTILANG KPR PEPABRI Lk. IV KEL. BULIAN KEC. BAJENIS',2021,2026,'LSM','-','-','-','AHU-0015922.AH.01.07 TAHUN 2018','-'),
	(188,'LEMBAGA PEMBERDAYAAN ANAK INDONESIA (LPAI)','JL.A.YANI NO 3',2003,2006,'LSM','-','-','-','-','-'),
	(189,'LEMBAGA PEMBERDAYAAN MASYARAKAT TEBING TINGGI','JL. L.SIKAPING 32',2002,2005,'LSM','-','-','-','-','-'),
	(190,'LEMBAGA PEMBERDAYAAN QURAN (LPQ) KOTA TEBING TINGGI','JL. ASRAMA KODIM 12',2010,2015,'ORMAS AGAMA','-','-','-','-','-'),
	(191,'LEMBAGA PENANGGULANGAN BENCANA ALAM DAN KESEJAHTERAAN RI','JL. PUS.PEJUANG NO. 2A',2012,2017,'LSM','-','-','-','-','-'),
	(192,'LEMBAGA PENGAWAS INDEPENDEN INDONESIA (LPII)','JL. PAHLAWAN',2013,2018,'LSM','-','-','-','-','-'),
	(193,'LEMBAGA PENGAWAS PEMBANGUNAN INDONESIA','JL. KF TENDEAN LK.II',2005,2010,'LSM','-','-','-','-','-'),
	(194,'LEMBAGA PENGEMBANGAN POTENSI NELAYAN MASYARAKAT PESISIR DAN KEHUTANAN','JL. MASJID NO 42D',2002,2007,'ORMAS PROFESI','-','-','-','-','-'),
	(195,'LEMBAGA PENGKAJI PROYEK APBD-N SE INDONESIA (LEPRO) KOTA TEBING TINGGI','JL. SEC.BERINGIN LK.4',2011,2015,'LSM','-','-','-','-','-'),
	(196,'LEMBAGA SOLIDARITAS PUTRA-PUTRI BANGSA','JL. P. SUMATERA, LK. 06',2012,2017,'LSM','-','-','-','-','-'),
	(197,'LEMBAGA SOSIAL BINA BANGSA MANDIRI KOTA TEBING TINGGI','JL. ABDUL HAMID NO 1',2010,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(198,'LEMBAGA STUDI KEGIATAN MASYARAKAT DAN NEGARA','JL. KEBUN,    LK. 03',2012,2017,'LSM','-','-','-','-','-'),
	(199,'LPKSN','JL.SETIA BUDI NO.160',2013,2018,'LSM','-','-','-','-','-'),
	(200,'LSM ANTI NAPZA','JL. P. SUMATERA, NO. 09',2012,2017,'LSM','-','-','-','-','-'),
	(201,'LSM DARI ORANG MINORITAS','JL. YOS SUDARSO 141',2003,2007,'LSM','-','-','-','-','-'),
	(202,'LSM TORPEDO','JL.PAHLAWAN NO.17',2013,2018,'LSM','-','-','-','-','-'),
	(203,'LUMBUNG INFORMASI MASYARAKAT KOTA  (LIMA KATA)','JL. KF.TANDEAN NO. 28',2009,2014,'LSM','-','-','-','-','-'),
	(204,'MAJELIS ADAT BUDAYA MELAYU (MABMI)','JL. HAMKA NO 3',2009,2014,'ORMAS KEGIATAN','-','-','-','-','-'),
	(205,'MAJELIS DAKWAH ISLAMIYAH (MDI)','JL. NENAS NO 30',1994,1999,'ORMAS AGAMA','-','-','-','-','-'),
	(206,'MAJELIS PIMPINAN CABANG PEMUDA PANCASILA KOTA TEBING TINGGI','Jln. KF. Tandean No. 262, Kota Tebing Tinggi',2019,2023,'ORMAS PROFESI','-','-','-','AHU–0001222.AH.01.08 TAHUN 2020','-'),
	(207,'MAJELIS PIMPINAN ICMI MUDA KOTA TEBING TINGGI','JLN. P. SUMATERA NO. 71. A KEL. TUALANG KEC. PADANG HILIR',2017,2022,'ORMAS PROFESI','-','-','-','AHU-0030621. AH.01.07 TAHUN 2016','-'),
	(208,'MAJELIS ULAMA INDONESIA (MUI)','JL. PENDIDIKAN NO 4',2009,2014,'ORMAS AGAMA','-','-','-','-','-'),
	(209,'MIO TEBING FANS CLUB KOTA TEBING TINGGI','JL. PAHLAWAN NO 16',2009,2010,'ORMAS PROFESI','-','-','-','-','-'),
	(210,'MOESLIM YOUTH CLUB KOTA TEBING TINGGI','JL. TAMRIN NO 141A',2011,2016,'ORMAS KEGIATAN','-','-','-','-','-'),
	(211,'MUSLIMAT AL WASHLIYAH','JL. 13 DESEMBER',2016,2021,'ORMAS AGAMA','-','-','-','-','-'),
	(212,'MUSYAWARAH KELUARGA GOTONG ROYONG (MKGR)','JL. K.PANE  NO 226',2006,2011,'ORMAS KEGIATAN','-','-','-','-','-'),
	(213,'NAHDLATUL ULAMA (NU)','Jl. YOS SUDARSO',2011,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(214,'NASYIYATUL WASLIYAH','JL. S.M. RAJA NO. 47',1997,2002,'ORMAS WANITA','-','-','-','-','-'),
	(215,'ORGANDA KOTA TEBING TINGGI','JL. YOS SUDARSO NO 34',2011,2016,'ORMAS KEGIATAN','-','-','-','-','-'),
	(216,'ORGANISASI AMATIR RADIO INDONESIA (ORARI) KOTA TEBING TINGGI','Jl. Abdul Hamid Lk IV, Kel. Bagelen, Kec. Padang Hilir',2022,2025,'ORMAS KEGIATAN','-','-','-','AHU-0000173.AH.01.08 TAHUN 2022','-'),
	(217,'ORGANISASI KARYAWAN UMAT PENTAKOSTA INDONESIA','JL. GATOT SUBROTO  NO 155',2000,2005,'ORMAS AGAMA','-','-','-','-','-'),
	(218,'PAGUYUBAN KELUARGA BESAR \"PUJAKESUMA\"','JL.G.MARTIMABANG NO70',2020,2025,'ORMAS KEGIATAN','-','-','-','-','-'),
	(219,'PAGUYUBAN NGESTI TUNGGAL (PANGESTU)','GANG IKHLAS NO 9',1995,2000,'ORMAS KEGIATAN','-','-','-','-','-'),
	(220,'PAGUYUBAN SOSIAL MARGA TIONGHOA INDONESIA (PSMTI)','JL. TERI NO 11',2011,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(221,'PAGUYUBAN WARTAWAN \"RAMADAN SEJAHTERA\"','JL. D.SUNDORO NO 10B',2006,2009,'ORMAS KEGIATAN','-','-','-','-','-'),
	(222,'PALU MONITOR INDONESIA (PAMID)','JL. KUTILANG Lingk IV, Gg BUNGA MELATI No. 9 A',2015,2020,'LSM','-','-','-','-','-'),
	(223,'PANJATRA','GANG KELUARGA NO 24',2007,2011,'ORMAS KEGIATAN','-','-','-','-','-'),
	(224,'PARISADA BUDHA DARMA NICIREN SYOSYU INDONESIA','JL. PERSATUAN NO.29',2008,2012,'ORMAS AGAMA','-','-','-','-','-'),
	(225,'PARSADAAN POMPARAN RAJA NAIRASAON (PPRN)','JL. IMAM BONJOL NO 60D',2005,2010,'ORMAS KEGIATAN','-','-','-','-','-'),
	(226,'PARTUHA MAUJANA SIMALUNGUN (PMS)','JLn.Jend.Gatot Subroto Lk I, Kel. Lubuk Baru',2021,2026,'ORMAS KEGIATAN','-','-','-','-','-'),
	(227,'PATRIOT NASIONAL (PATRON)','JL. YOS SUDARSO NO 45',2008,2013,'ORMAS KEGIATAN','-','-','-','-','-'),
	(228,'PATRIOT PANCA MARGA ( PPM )','JL. TENGGIRI NO 24',2007,2012,'ORMAS KEGIATAN','-','-','-','-','-'),
	(229,'PEDANG KEADILAN PERJUANGAN (PKP)','JL. BAHBOLON LK.VII',2009,2014,'LSM','-','-','-','-','-'),
	(230,'PELAJAR ISLAM INDONESIA (PII)','JL. IMAM BONJOL NO11',2003,2005,'ORMAS PEMUDA','-','-','-','-','-'),
	(231,'PEMANGKU ADAT KERAJAAN NEGERI PADANG DELI (LPAKNPD)','JLN. JENDERAL R. SUPRAPTO NO. 126 / MESJID RAYA',2018,2023,'ORMAS KEGIATAN','-','-','-','-','-'),
	(232,'PEMANTAU KINERJA APARATUR NEGARA','Jl. AHMAD YANI NO.40C',2011,2012,'LSM','-','-','-','-','-'),
	(233,'PEMANTAU KORUPSI DAN PENYELAMAT HARTA NEGARA','JL. CEMPAKA UJUNG',2011,2015,'LSM','-','-','-','-','-'),
	(234,'PEMBERANTASAN KORUPSI RI','JL. DR. HAMKA',2013,2018,'LSM','-','-','-','-','-'),
	(235,'PEMERHATI KINERJA PERKEBUNAN DAN APARATUR NEGARA RI','GRIYA BULIAN  BLOK A NO 80',2014,2019,'LSM','-','-','-','-','-'),
	(236,'PEMERSATU WARTAWAN TEBING TINGGI (PEWARTA)','GANG SAUDARA NO 7',2002,2007,'ORMAS PROFESI','-','-','-','-','-'),
	(237,'PEMUDA GEREJA KRISTEN PROTESTAN SIMALUNGUN','JL. P.SAMOSIR NO 19',2002,2005,'ORMAS PEMUDA','-','-','-','-','-'),
	(238,'PEMUDA KATOLIK ','JLN. PAHLAWAN NO. 13',2022,2025,'ORMAS PEMUDA','-','-','-','-','-'),
	(239,'PEMUDA MITRA KAMTIBMAS KOTA TEBING TINGGI','JL. KF.TANDEAN N0 122',2009,2014,'ORMAS KEGIATAN','-','-','-','-','-'),
	(240,'PEMUDA MUHAMMADIYAH','JL. SM.RAJA NO 47',2012,2014,'ORMAS PEMUDA','-','-','-','-','-'),
	(241,'PEMUDA PANCA MARGA (PPM)','KORAMIL 13',2007,2011,'ORMAS PEMUDA','-','-','-','-','-'),
	(242,'PENEGAK HAK-HAK RAKYAT MANDIRI','JL. YOS SUDARSO NO 77',2006,2011,'LSM','-','-','-','-','-'),
	(243,'PENGURUS CABANG 0204 FKPPI KOTA TEBING TINGGI','JL. JEND. SUDIRMAN NO 257',2018,2023,'ORMAS KEGIATAN','-','-','-','AHU-0072303-AH.0107 TAHUN 2016','-'),
	(244,'PENGURUS CABANG MAJELIS BUDDHAYANA INDONESIA KOTA TEBING TINGGI','Jl. Veteran, No. 40, Kel. Tebing Tinggi Lama, Kec. Tebing Tinggi Kota, Kota Tebing Tinggi',2022,2025,'ORMAS AGAMA','-','-','-','AHU-0000330.AH.01.08 TAHUN 2019','-'),
	(245,'PENGURUS CABANG MAJELIS PANDITA BUDDHA MAITREYA INDONESIA (MAPANBUMI)','JL. BADAK NO. 54 KEL. BADAK BEJUANG KEC. T.TINGGI KOTA',2020,2025,'ORMAS AGAMA','-','-','-','AHU-0016118.AH.01.07 TAHUN 2020','-'),
	(246,'PENGURUS DAERAH BADAN KONTAK MAJELIS TAKLIM (BKMT) KOTA TEBING TINGGI ','Jln. Persatuan No.41 Y Tebing Tinggi',2018,2023,'ORMAS AGAMA','-','-','-','AHU–0010613.AH.01.07 TAHUN 2017','-'),
	(247,'PENGURUS DAERAH KESATUAN AKSI MAHASISWA MUSLIM INDONESIA SERDANG BEDAGAI-TEBING TINGGI','Jl. Sofyan Zakaria Lk. II, Kel. Tebing Tinggi, Kec. Padang Hilir',2021,2023,'ORMAS PEMUDA','-','-','-','AHU-0000372.AH.01.08 TAHUN 2020','-'),
	(248,'PENGURUS DAERAH NASIONAL DEMOKRAT KOTA TEBING  TINGGI','JL. AMD NO 9',2010,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(249,'PENGURUS DAERAH WANITA ISLAM KOTA TEBING TINGGI','Jl. Prof. Dr. Hamka, No. 62, Kel. Bulian, kec. Bajenis, Kota Tebing Tinggi',2020,2025,'ORMAS WANITA','-','-','-','AHU-0013162.AH.01.07 TAHUN 2018','-'),
	(250,'PEREMPUAN AMANAT NASIONAL (PUAN)','JL. A.YANI NO 37',2008,2013,'ORMAS WANITA','-','-','-','-','-'),
	(251,'PERGURUAN KARATE DO TAKO INDONESIA','JL. DELIMA NO 3',2007,2012,'ORMAS KEGIATAN','-','-','-','-','-'),
	(252,'PERHIMPUNAN KELUARGA BESAR PELAJAR ISLAM INDONESIA','JL. A.BILAL NO 37',2010,2013,'ORMAS KEGIATAN','-','-','-','-','-'),
	(253,'PERHIMPUNAN PEDAGANG PASAR KAIN / BUNGA (P3KB)','JL. D.I PANJAITAN NO. 45 TEBING TINGGI',2017,2022,'ORMAS PROFESI','-','-','-','SKT KEMENDAGRI NO. 0215-00-00/150/XII/2017','-'),
	(254,'PERHIMPUNAN PEDAGANG PUSAT PASAR KAIN (P4K) KOTA TEBING TINGGI','JLN. SELAT SUNDA NO. 94 KEL. MANDAILING KEC. TEBING TINGGI KOTA TEBING TINGGI ',2017,2022,'ORMAS PROFESI','-','-','-','SKT KEMENDAGRI NO. 0215-00-00/149/XII/2017','-'),
	(255,'PERKUMPULAN LEMBAGA KHATULISTIWA PENDIDIKAN DAN PELATIHAN MANAJEMEN','JLN.. PULAU SUMATERA GANG AMALIA No. 79 KOTA TEBIUNG TINGGI',2019,2024,'ORMAS PENDIDIKAN','-','-','-','AHU-0001263.AH.01.07  TAHUN 2019','-'),
	(256,'PERKUMPULAN LEMBAGA PEDULI PEMBANGUNAN DAN ASET SEJARAH REPUBLIK INDONESIA (DPC.LSM.LPPAS-RI)','Jl. Ahmad Yani No. 3 Lk IX Kel. Bandarsono Kec. Padanghulu',2023,2024,'LSM','-','-','-','AHU-0012458.AH.01.07 TAHUN 2022','-'),
	(257,'PERKUMPULAN LEMBAGA PERLINDUNGAN ANAK (LPA)','JL. SISINGAMANGARAJA No. 14 KOTA T. TINGGI                                                ',2019,2021,'ORMAS KEGIATAN','-','-','-','AHU-0074042.AH.01.07 TAHUN 2016','-'),
	(258,'PERKUMPULAN LSM SWARA SEMESTA KEADILAN','JL. Ir. JUANDA LINGKUNGAN I KEL. KARYA JAYA',2018,2023,'LSM','-','-','-','AHU-0000381.AH.01.07 TAHUN 2020','-'),
	(259,'PERKUMPULAN MINYAK TANAH AGEN SUSANA','JL. SUDIRMAN NO 331',2003,2006,'ORMAS KEGIATAN','-','-','-','-','-'),
	(260,'PERKUMPULAN PADEPOKAN PENCAK SILAT TENAGA DALAM (PSTD) BUDI SUCI INDONESIA KOTA T.TINGGI','JLN. SAKTI LUBIS NO.15 KEL. PASAR BARU, KEC.TEBING TINGGI KOTA KOTA TEBING TINGGI',2020,2025,'ORMAS KEGIATAN','-','-','-','AHU-0005053.AH.01.07 TAHUN 2020','-'),
	(261,'PERKUMPULAN PEDAGANG PAKAIAN BEKAS (PPPB)','JL. KUBU NO 5',2003,2007,'ORMAS PROFESI','-','-','-','-','-'),
	(262,'PERKUMPULAN PERSAUDARAAN PUTRA SOLO CABANG TEBING TINGGI','Jl. Asrama Gg. Madrasah Lk. IV, Kel Persiakan, Kec. P. Hulu, Kota Tebing Tinggi',2019,2024,'ORMAS KEGIATAN','-','-','-','AHU-0000615.AH.01.07 TAHUN 2015','-'),
	(263,'PERKUMPULAN PERUSAHAAN MEDIA ONLINE INDONESIA (MOI) KOTA TEBING TINGGI','Jl. Imam Bonjol Kelurahan Tebing Tinggi Lama',2021,2024,'ORMAS KEGIATAN','-','-','-','AHU-0011601.AH.01. 07 TAHUN 2018','-'),
	(264,'PERKUMPULAN PETUALANG ALAM BEBAS (PETALA)  KOTA TEBING TINGGI                              ','JLN. GATOT SUBROTO KEL. PABATU, KEC. PADANG HULU KOTA TEBING TINGGI',2018,2020,'ORMAS KEGIATAN','-','-','-','AHU-0009770.AH.01.07 TAHUN 2018','-'),
	(265,'PERKUMPULAN RADIO ANTAR PENDUDUK INDONESIA','JLn. Asrama Kodim Lk. VI Kel. Persiakan',2020,2024,'ORMAS KEGIATAN','-','-','-','AHU-0000704.AH.01.08 TAHUN 2020','-'),
	(266,'PERKUMPULAN SOSIAL PANTI JOMPO DAN PERKUBURAN TIONGHOA','JL. ISKANDAR MUDA NO.83',2004,2009,'ORMAS KEGIATAN','-','-','-','-','-'),
	(267,'PERKUMPULAN SOSIAL WARGA TEOC HEW INDONESIA','JL. TERI NO 11',2003,2006,'ORMAS KEGIATAN','-','-','-','-','-'),
	(268,'PERSATUAN ABANG BECA (PAB)','JL. BAHBOLON NO 110',1998,2003,'ORMAS PROFESI','-','-','-','-','-'),
	(269,'PERSATUAN GURU REPUBLIK INDONESIA (PGRI)','JL. YOS SUDARSO LK III',2020,2025,'ORMAS PROFESI','-','-','-','-','-'),
	(270,'PERSATUAN GURU SWASTA INDONESIA (PGSI) KOTA TEBING TINGGI','JL. S.BAHILANG NO 5',2010,2015,'ORMAS PROFESI','-','-','-','-','-'),
	(271,'PERSATUAN ISTRI VETERAN RI ( PIVERI RI )','JL. PATTIMURA NO 75',2009,2014,'ORMAS WANITA','-','-','-','-','-'),
	(272,'PERSATUAN PENSIUNAN KARYAWAN KERETA API','JL. SUTOYO NO 14',2007,2012,'ORMAS KEGIATAN','-','-','-','-','-'),
	(273,'PERSATUAN PURNAWIRAWAN DAN WKAURI TNI-POLRI (PEPABRI)','JL. PATTIMURA NO 75',2011,2016,'ORMAS KEGIATAN','-','-','-','-','-'),
	(274,'PERSATUAN PURNAWIRAWAN TNI ANGKATAN DARAT','Jl. Sutomo No. 6 Lk. II, Kel. Rambung, Kec. Tebing Tinggi Kota',2022,2027,'ORMAS KEGIATAN','-','-','-','AHU-0000523.AH.01.08 TAHUN 2022','-'),
	(275,'PERSATUAN SOBAT BATAK KOTA TEBING TINGGI (PARSOBAT)','JL. TERI NO 8',2010,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(276,'PERSATUAN TARBIYAH ISLAMIYAH (PTI)','JL. SUNDORO NO 26A',1992,1997,'ORMAS AGAMA','-','-','-','-','-'),
	(277,'PERSATUAN TUNA NETRA INDONESIA (PERTUNI)','KOMPLEK BALAI DEWA',2018,2023,'ORMAS PROFESI','-','-','-','AHU-0068905.AH.01.07 TAHUN 2016','-'),
	(278,'PERSATUAN WANITA KRISTEN INDONESIA (PWKI)','JL. KEMERDEKAAN NO 23',2007,2012,'ORMAS WANITA','-','-','-','-','-'),
	(279,'PERSATUAN WANITA TARBIYAH ISLAMIYAH (PWTI)','JL. PANCASILA NO. 11',1992,1997,'ORMAS WANITA','-','-','-','-','-'),
	(280,'PERSIT KARTIKA CANDRA KIRANA RANTING 14','ASRAMA KODIM 0204 DS',2010,2015,'ORMAS WANITA','-','-','-','-','-'),
	(281,'PIJAR KEADILAN','JL. IMAM BONJOL NO.43',2009,2014,'LSM','-','-','-','-','-'),
	(282,'PIMPINAN CABANG MUSLIMAT NAHDATUL ULAMA KOTA TEBING TINGGI','JL. P.IRIAN LK.IV',2010,2015,'ORMAS AGAMA','-','-','-','-','-'),
	(283,'PIMPINAN DAERAH AL JAMIYATUL WASHLIYAH KOTA TEBING TINGGI','JLN. 13 DESEMBER NO. 3 KOTA TEBING TINGGI',2020,2025,'ORMAS AGAMA','-','-','-','AH0000328.AH.01.08 TAHUN 2019','-'),
	(284,'PIMPINAN DAERAH IKATAN PELAJAR AL-WASHLIYAH KOTA TEBING TINGGI','JLN. 13 DESEMBER NO. 3 KOTA TEBING TINGGI',2020,2022,'ORMAS AGAMA','-','-','-','AHU–0000205.AH.01.08 TAHUN 2020','-'),
	(285,'PIMPINAN DAERAH MUHAMMADIYAH','JL. SM.RAJA NO.47',2010,2015,'ORMAS KEGIATAN','-','-','-','-','-'),
	(286,'PIMPINAN DAERAH PERSATUAN UMMAT ISLAM (PUI) KOTA TEBING TINGGI','Jln. Badak No.17, Kel. Badak Bejuang, Kec. Tebing Tinggi Kota',2022,2027,'ORMAS AGAMA','-','-','-','AHU–0000205.AH.01.08 TAHUN 2020','-'),
	(287,'POTRET','JL. KF TENDEAN',2004,2009,'LSM','-','-','-','-','-'),
	(288,'PRO JOKOWI','JL. LETDA SUJONO PERUMAHAN BULIAN PERMAI',2014,2019,'ORMAS KEGIATAN','-','-','-','-','-'),
	(289,'PUNGUAN RAJA PANJAITAN,  BORU,  BERE,  DOHOT IBEBERE','JL. BHAKTI NO 68 Lk III Kel. Satria Kec. Padang Hilir',2023,2027,'ORMAS KEGIATAN','-','-','-','AHU-0092 44.AH.01.07 TAHUN 2022','-'),
	(290,'PUSAT INKUBASI USAHA KECIL KOTA TEBING TINGGI','PONDOK SRI PADANG',2004,2007,'ORMAS KEGIATAN','-','-','-','-','-'),
	(291,'RADIO ANTAR PENDUDUK INDONESIA (RAPI)','JL. SUNDORO NO 35',2009,2012,'ORMAS KEGIATAN','-','-','-','-','-'),
	(292,'RELAWAN TIGA PILAR','JL. DARAT NO.27A',2010,2015,'LSM','-','-','-','-','-'),
	(293,'REMPALA INDONESIA KOTA TEBING TINGGI','JL. KOPERASI NO 126',2013,2018,'ORMAS KEGIATAN','-','-','-','-','-'),
	(294,'SANGGAR SENI PESONA NUSANTARA TEBING TINGGI','JL. Nangka Gg. Embacang Lk I, Kel. Rambung, Kec. Tebing Tinggi Kota',2020,2023,'ORMAS KESENIAN','-','-','-','AHU-0007634.AH.01.07 TAHUN 2020','-'),
	(295,'SATUAN KARYA ULAMA INDONESIA (SKUI)','JL. DAMAR LAUT NO 138',1994,1997,'ORMAS KEGIATAN','-','-','-','-','-'),
	(296,'SATUAN SISWA PELAJAR DAN MAHASISWA PEMUDA PANCASILA KOTA T. TINGGI','JL. K.F TANDEAN, NO.88 KOTA T. TINGGI',2018,2020,'ORMAS PEMUDA','-','-','-','AHU-0004418.AH.01.07 TAHUN 2017','-'),
	(297,'SATUAN TUGAS JOKO TINGKIR KOTA TEBING TINGGI','JL. SOFYAN ZAKARIA 2A',2012,2017,'ORMAS KEGIATAN','-','-','-','-','-'),
	(298,'SEKOCI INDORATU','JL. MERANTI NO. 16',2012,2017,'LSM','-','-','-','-','-'),
	(299,'SENKOM MITRA POLRI TEBING TINGGI','JL.P. SUMATERA NO. 55',2013,2018,'LSM','-','-','-','-','-'),
	(300,'SENTRA TRANSFORMASI DAN TRANSPARANSI NEGERI (DPK LSM STRATEGI)','JL. YOS SUDARSO NO. 35',2019,2024,'LSM','-','-','-','SKT KEMENDAGRI NO. 0200-00.00/567/X/2019','-'),
	(301,'SERIKAT BURUH SEJAHTERA INDONESIA','JL. IMAM BONJOL',2007,2011,'ORMAS PROFESI','-','-','-','-','-'),
	(302,'SERIKAT BURUH SEJAHTERA INDONESIA SEKTOR TRANSPORTASI DAN ANGKUTAN (SBSI-SPA)','JL. DANAU TOBA NO 7',2009,2013,'ORMAS PROFESI','-','-','-','-','-'),
	(303,'SERIKAT PEKERJA PERKEBUNAN UNIT RS.SRI PAMELA','JL. SUDIRMAN NO 299',2001,2005,'ORMAS PROFESI','-','-','-','-','-'),
	(304,'SERIKAT PERS RI','JL. ASRAMA NO.100',2013,2018,'ORMAS PROFESI','-','-','-','-','-'),
	(305,'SERIKAT SOSIAL PEMUK JEMAAT KHATOLIK','JL. PAHLAWANG NO. 13',2003,2008,'ORMAS AGAMA','-','-','-','-','-'),
	(306,'SOKA GAKKAI INDONESIA','JL. KF. TANDEAN BISNIS SENTRAL NO.5',2016,2021,'ORMAS AGAMA','-','-','-','-','-'),
	(307,'SUTEO LESTARI','JL. SUPRAPTO 105 / 01',2007,2012,'YAYASAN','-','-','-','-','-'),
	(308,'TEAM OPERASIONAL PENYELAMATAN ASSET NEGARA REPUBLIK INDONESIA (TOPAN - RI)','JL. KUTILANG Llk.4   Gg. BUNGA MELATI NO. 9 A                                                              ',2015,2020,'LSM','-','-','-','-','-'),
	(309,'TEBING TINGGI SCOOTER (TTS)','JL. PALA LK . III',2015,2020,'ORMAS KEGIATAN','-','-','-','-','-'),
	(310,'TEBING TINGGI VESVA CLUB (TT-VC)','JL. HAMKA NO 100F',2006,2010,'ORMAS KEGIATAN','-','-','-','-','-'),
	(311,'TOPAN RI ','Gg. JAMBU NO 1',2006,2009,'LSM','-','-','-','-','-'),
	(312,'TOPPAN RI','JL. P. BELITUNG NO. 13',2013,2018,'LSM','-','-','-','-','-'),
	(313,'WANITA KHATOLIK CABANG TEBING TINGGI ','JL. PAHLAWAN NO 13',2009,2012,'ORMAS AGAMA','-','-','-','-','-'),
	(314,'YAYASAN  13  DESEMBER  KOTA  TEBING  TINGGI','JL. PEPAYA  NO. 2',2005,2008,'YAYASAN','-','-','-','-','-'),
	(315,'YAYASAN  LAPI  NAIM','JL. DR. HAMKA  NO. 7C',2004,2009,'YAYASAN','-','-','-','-','-'),
	(316,'YAYASAN  MANDALA NUSANTARA  MANDIRI  (YMNM)','JL. DR. HAMKA NO. 3',2003,2007,'YAYASAN','-','-','-','-','-'),
	(317,'YAYASAN  PERDULI  HIV DAN ANTI NARKOBA','JL. T. HASYM  NO. 16',2001,2005,'YAYASAN','-','-','-','-','-'),
	(318,'YAYASAN  PERSATUAN  PERSAUDARAAN PUTRA SOLO','JL. ASRAMA KODIM LK. VI',2009,2014,'YAYASAN','-','-','-','-','-'),
	(319,'YAYASAN  SAKYA  MUKI ','JL. VETERAN NO. 40',1999,2004,'YAYASAN','-','-','-','-','-'),
	(320,'YAYASAN  TERATAI','JL. A. YANI  NO.199',2001,2005,'YAYASAN','-','-','-','-','-'),
	(321,'YAYASAN  VIHARA  MADANA ','JL. VETERAN NO. 41',2011,2016,'YAYASAN','-','-','-','-','-'),
	(322,'YAYASAN \"HORIZON\" KOTA TEBING TINGGI','JL. SINABUNG NO 28',2010,2015,'YAYASAN','-','-','-','-','-'),
	(323,'YAYASAN ABULYATAMA INDONESIA CABANG TEBING TINGGI','JL. Gunung Leuseur Blok A2 Lk II Kel. Tanjung Marulak, Kec. Rambutan',2021,2024,'YAYASAN','-','-','-','AHU–0006853.AH.01.04 TAHUN 2015','-'),
	(324,'YAYASAN BANTUAN HUKUM SUMATERA TIMUR TEBING TINGGI','JL. Ahmad Yani No. 47 Lk II Kel. Durian, Kec. Bajenis',2022,2025,'YAYASAN','-','-','-','AHU–0008273.AH.01.04 TAHUN 2022','-'),
	(325,'YAYASAN BINA PEMUDA KOTA TEBING TINGGI','GANG ARJUNA NO 3',2011,2015,'YAYASAN','-','-','-','-','-'),
	(326,'YAYASAN CAHAYA MUSLIMAH KOTA TEBING TINGGI','JL. MANDAILING NO.101',2010,2015,'YAYASAN','-','-','-','-','-'),
	(327,'YAYASAN GENERASI MAJAPAHIT  PANCASILA (GEMPA)','JL. SISINGAMANGARAJA KOMPLEK CITRA HASYIM BLOK A  NO. 22 A',2020,2025,'YAYASAN','-','-','-','AHU–0014997.AH.01.04 TAHUN 2020','-'),
	(328,'YAYASAN HARAPAN UMAT','JL. DR. HAMKA  NO 1',2010,2015,'YAYASAN','-','-','-','-','-'),
	(329,'YAYASAN INSAN SYAMIL MULIA KOTA TEBING TINGGI','Jl. BUKIT CERMIN Kel. MEKAR SENTOSA',2019,2024,'YAYASAN','-','-','-','AHU–0003536.AH.01.04 TAHUN 2019','-'),
	(330,'YAYASAN ISLAM AT TAKRIR KOTA TEBING TINGGI','JL. Besi Lk III No. 5, Kel. Tambangan, Kec. Padang Hilir',2021,2025,'YAYASAN','-','-','-','AHU–0019520.AH.01.04 TAHUN 2021','-'),
	(331,'YAYASAN KERAPATAN ZURIAT DIRAJA MELAYU NEGERI PADANG KOTA TEBING TINGGI','JL. PALA LK.III, KELURAHAN BANDAR UTAMA',2021,2026,'YAYASAN','-','-','-','AHU–0011400.AH.01.04 TAHUN 2021','-'),
	(332,'YAYASAN MEDIA SWARA SEMESTA','JL. MERPATI KEL. PINANG MANCUNG T. TINGGI',2018,2023,'YAYASAN','-','-','-','AHU-0014268.AH.01.04 TAHUN 2018','-'),
	(333,'YAYASAN PANTI ASUHAN SELFAN KOTA TEBING TINGGI','JL. LETDA SUJONO LK I, KEL. TELUK KARANG, KEC. BAJENIS',2021,2025,'YAYASAN','-','-','-','AHU–0027711.AH.01.04 TAHUN 2021','-'),
	(334,'YAYASAN PENDIDIKAN ISLAM MUTIARA IBU','Jl. K. PANE  NO 5',2009,2014,'YAYASAN','-','-','-','-','-'),
	(335,'YAYASAN PENDIDIKAN SOSIAL DAKWAH ISLAM MUTIARA','JL. K. PANE  NO 5',2006,2011,'YAYASAN','-','-','-','-','-'),
	(336,'YAYASAN PERGURUAN ISLAM TERPADU PERMATA HATI KOTA TEBING TINGGI','JL. ABD HAMID LK.1',2011,2015,'YAYASAN','-','-','-','-','-'),
	(337,'YAYASAN UMMATAN WAHIDAH KOTA TEBING TINGGI','JL. THAMRIN, GG TURI',2011,2015,'YAYASAN','-','-','-','-','-');

/*!40000 ALTER TABLE `morganisasi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpegawai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpegawai`;

CREATE TABLE `mpegawai` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `PegNoUrut` int(11) DEFAULT NULL,
  `PegNama` varchar(50) DEFAULT NULL,
  `PegNIP` varchar(50) DEFAULT NULL,
  `PegJabatan` varchar(200) DEFAULT NULL,
  `PegKategori` varchar(50) DEFAULT NULL,
  `PegIsAktif` tinyint(1) DEFAULT '1',
  `PegImgPath` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpegawai` WRITE;
/*!40000 ALTER TABLE `mpegawai` DISABLE KEYS */;

INSERT INTO `mpegawai` (`Uniq`, `PegNoUrut`, `PegNama`, `PegNIP`, `PegJabatan`, `PegKategori`, `PegIsAktif`, `PegImgPath`)
VALUES
	(2,1,'ZUBIR HUSNI HARAHAP, SH','196412251986021003','Kepala Badan','ASN',1,'pp-01-zubir.jpeg'),
	(6,2,'ERWANDI, SH','196601071986021001','Sekretaris','ASN',1,'pp-02-erwandi.png'),
	(7,3,'MISNIWATI, SH','196510161993032004','Kabid Kewaspadaan Nasional dan Penanganan Konflik','ASN',1,'pp-03-misniwati.png'),
	(8,4,'SYAFRIDA PANGGABEAN, SH','196809022002122001','Kabid Ideologi, Wawasan Kebangsaan dan Ketahanan Ekonomi, Sosial, Budaya dan Agama','ASN',1,'pp-04-syafrida.jpeg'),
	(9,5,'ZULFADLI MATONDANG, S.Sos','198004102009031002','Kabid Politik Dalam Negeri dan Organisasi Kemasyarakatan','ASN',1,'pp-05-zulfadli.jpeg'),
	(10,6,'KHAIRIN NAZRI, SE','198401262008011001','Kasubbag Umum dan Kepegawaian','ASN',1,'pp-6-khairin.jpeg'),
	(11,7,'NELSON SITORUS, SH','196705211993121001','Analis Kebijakan Muda','ASN',1,'pp-07-nelson.png'),
	(12,8,'PINTAULI, SH','196903151997032001','Analis Kebijakan Muda','ASN',1,'pp-08-pintauli.png'),
	(13,9,'HAMIDAH LUBIS, SH','197305211997032001','Analis Kebijakan Muda','ASN',1,'pp-09-hamidah.jpeg'),
	(14,10,'SRI DEWI ASTUTI, SE','197805132005022002','Analis Keuangan Pusat dan Daerah Muda','ASN',1,'pp-10-sridewi.png'),
	(15,11,'CHAIRUL AMRI LUBIS, SE','197901142009011006','Analis Kebijakan Muda','ASN',1,'pp-11-amri.jpeg'),
	(16,12,'MARTHIN ALEXANDER ADRIAN, SH','198407012008041001','Analis Kebijakan Muda','ASN',1,'pp-12-marthin.png'),
	(17,13,'DIAN HALIMATUSA\'DIAH NASUTION, A.Md','198611192010012023','Bendahara','ASN',1,'pp-13-dian.jpeg'),
	(18,14,'NOVI ANGGRAINI, S.E','199511122019032006','Analis Laporan Keuangan','ASN',1,'pp-14-novi.jpeg'),
	(19,15,'AULIA MA’RUF HARAHAP, S.Tr.IP','199711112021081001','Analis Data dan Informasi','ASN',1,'pp-15-aulia.png'),
	(20,16,'KARINA SARAGIH, S.E','198810142022032001','Analis Ketahanan Ekonomi','ASN',1,NULL),
	(21,16,'MUHAMMAD FADLI','197712062002121005','Pengadministrasi Umum','ASN',1,'pp-16-fadli.jpeg'),
	(22,17,'YOEL ROLAS SIMANJUNTAK, A.Md','199508122019031005','Pengelola Teknologi Informasi','ASN',1,'pp-17-yoel.png'),
	(23,18,'BAHTIAR SINAGA','197503312007011006','Pengolah Data Intelijen','ASN',1,'pp-18-bahtiar.png'),
	(24,20,'DODI HERIZAL','-','STAFF','NON ASN',1,NULL),
	(25,21,'MEI RINA K. HASIBUAN','-','STAFF','NON ASN',1,NULL),
	(26,22,'SATRIA SAGITA','-','STAFF','NON ASN',1,NULL),
	(27,23,'FARDIAN ISKANDAR LUBIS','-','STAFF','NON ASN',1,NULL),
	(28,24,'TRI RUKMANA MANDA SARI','-','STAFF','NON ASN',1,NULL),
	(29,25,'FANNI  ALVITANIA','-','STAFF','NON ASN',1,NULL),
	(30,26,'UMA  RUMAIYAH','-','STAFF','NON ASN',1,NULL),
	(31,27,'SARAH ARIFAH NADIRA SIREGAR','-','STAFF','NON ASN',1,NULL);

/*!40000 ALTER TABLE `mpegawai` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tformulir
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tformulir`;

CREATE TABLE `tformulir` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `FormNama` varchar(200) DEFAULT '',
  `FormKeterangan` varchar(200) NOT NULL DEFAULT '',
  `FormAttachment` varchar(200) DEFAULT '',
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tlapor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tlapor`;

CREATE TABLE `tlapor` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `LaporNama` varchar(200) NOT NULL DEFAULT '',
  `LaporNIK` varchar(200) DEFAULT NULL,
  `LaporHP` varchar(50) NOT NULL DEFAULT '',
  `LaporEmail` varchar(50) NOT NULL DEFAULT '',
  `LaporKategori` varchar(200) NOT NULL DEFAULT '',
  `LaporIsi` text NOT NULL,
  `LaporStatus` enum('DITERIMA','PROSES','SELESAI') NOT NULL DEFAULT 'DITERIMA',
  `LaporKeterangan` text,
  `CreatedOn` datetime NOT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `UpdateBy` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tlapor` WRITE;
/*!40000 ALTER TABLE `tlapor` DISABLE KEYS */;

INSERT INTO `tlapor` (`Uniq`, `LaporNama`, `LaporNIK`, `LaporHP`, `LaporEmail`, `LaporKategori`, `LaporIsi`, `LaporStatus`, `LaporKeterangan`, `CreatedOn`, `UpdatedOn`, `UpdateBy`)
VALUES
	(1,'Yoel Rolas Simanjuntak','1271031208950002','085359867032','yoelrolas@gmail.com','Lainnya','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','DITERIMA',NULL,'2023-05-08 23:33:24',NULL,NULL),
	(2,'Rolas','1271031208950002','085359867032','yoelrolas@gmail.com','Lainnya','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','PROSES','Laporan ini sedang diproses. Kami akan informasikan hasilnya segera. Terimakasih.','2023-05-08 23:33:40','2023-05-09 00:41:08','admin'),
	(3,'Partopi Tao','1271031208950002','085359867032','yoelrolas@gmail.com','Lainnya','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','SELESAI','Laporan anda telah ditindaklanjut oleh bidang terkait. Terimakasih atas partisipasi anda.','2023-05-08 23:34:25','2023-05-09 00:39:24','admin');

/*!40000 ALTER TABLE `tlapor` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
