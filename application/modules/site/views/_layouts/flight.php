<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?=trim(preg_replace('/\s+/', ' ', $this->setting_web_desc))?>">
  <meta name="author" content="Partopi Tao">
  <meta name="keyword" content="mpp, mal pelayanan publik, tebing tinggi, mpp tebing tinggi, partopi tao">
  <meta property="og:title" content="<?=$this->setting_web_name?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:image" content="<?=MY_IMAGEURL.'logo.png'?>" />
  <meta property="og:image:width" content="50" />
  <meta property="og:image:height" content="50" />
  <title><?=!empty($title) ? $this->setting_web_name.' - '.$title : $this->setting_web_name?></title>

  <link rel="stylesheet" href="<?=base_url()?>assets/themes/flight/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/flight/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/fonts/fontawesome-pro/web/css/all.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/flight/css/hero-slider.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/flight/css/owl-carousel.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/flight/css/datepicker.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/flight/css/tooplate-style.css">
  <link rel="shortcut icon" href="<?=MY_IMAGEURL.'favicon.png'?>" type="image/x-icon" />

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

  <script src="<?=base_url()?>assets/themes/flight/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

  <!-- Select 2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">

  <!-- Toastr -->
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

  <link href="<?=base_url()?>assets/themes/gotto/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/owl.theme.default.min.css" rel="stylesheet">

  <style>
  .se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(<?=base_url()?>assets/media/preloader/<?=$this->setting_web_preloader?>) center no-repeat #fff;
  }
  .banner .tabs-content p {
    margin-top: 0px;
    margin-bottom: 40px;
    font-size: 20px;
    color: #fff;
    font-weight: 600;
  }

  #fasilitas {
  	margin-top: 100px;
  	background-color: #f4f4f4;
  	padding: 80px 0px;
  }

  #fasilitas .tabs {
    list-style: none;
    margin-top: 0px;
    padding-right: 0px;
  }

  #fasilitas .tabs li:first-child {
  	margin-top: 0px;
  }

  #fasilitas .tabs li {
    display: block;
    text-align: center;
    margin: 8px 0px;
  }

  #fasilitas .tabs a {
    display:block;
    transition: all 0.5s;
    text-align: left;
    text-decoration:none;
    text-transform:uppercase;
    letter-spacing: 0.5px;
    color:#fff;
    font-size: 17px;
    font-weight: 700;
    display: inline-block;
    width: 100%;
    height: 60px;
    line-height: 60px;
    padding-left: 20px;
    background-color: #1f3646;
  }

  #fasilitas .tabs a em {
  	font-style: normal;
  	font-weight: 700;
  }

  #fasilitas .tabs a i {
  	float: right;
  	background-color: rgba(250, 250, 250, 0.2);
  	height: 60px;
  	width: 60px;
  	line-height: 60px;
  	text-align: center;
  	font-size: 36px;
  }

  #fasilitas .tabs .active {
    background-color: #074CAD;
    color: #fff;
  }

  #fasilitas .tabs .active i {
    background-color: rgba(255, 255, 255, 0.3);
  }
  #instansi {
  	margin-top: 100px;
  	margin-bottom: 100px;
  }
  #owl-instansi .owl-pagination {
    margin-top: 20px;
    opacity: 1;
    display: inline-block;
    position: relative;
    text-align: center;
    z-index: 9999;
    background-color: transparent;
    height: 0px;
    line-height: 0px;
    padding: 0px;
  }

  #owl-instansi .owl-page span {
    display: block;
    width: 10px;
    height: 10px;
    margin: 0px 5px;
    filter: alpha(opacity=50);
    opacity: 0.5;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border-radius: 20px;
    background: #ddd;
  }

  #owl-instansi .owl-controls .owl-page.active span,
  #owl-instansi .owl-controls.clickable .owl-page:hover span{
    filter: Alpha(Opacity=100);/*IE7 fix*/
    opacity: 1;
    display: inline-block;
    background: #074CAD;
  }

  #ruanglaktasi .text-content,#ruangbaca .text-content,#ruangbermain .text-content,#loketdifabel .text-content {
  	background-color: rgba(31, 54, 70, 0.9);
  	position: absolute;
  	bottom: 0;
  	right: 15px;
  	left: 15px;
  	height: 80px;
  	overflow: hidden;
  	color: #fff;
  	padding-left: 20px;
  }
  #ruanglaktasi .text-content h4,#ruangbaca .text-content h4,#ruangbermain .text-content h4,#loketdifabel .text-content h4 {
  	font-size: 19px;
  	font-weight: 700;
  	margin-top: 20px;
  	margin-bottom: 5px;
  }

  #ruanglaktasi .text-content span,#ruangbaca .text-content span,#ruangbermain .text-content span,#loketdifabel .text-content span {
  	font-size: 14px;
  	text-transform: uppercase;
  	color: #fff;
  	letter-spacing: 0.5px;
  }
  </style>
</head>
<body>
  <div class="se-pre-con"></div>
  <section class="banner" id="top" style="background-image: url('<?=MY_IMAGEURL.'img-bg-hero.jpeg'?>')">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="left-side">
              <div class="logo">
                <img src="<?=MY_IMAGEURL.'logo-secondary.png'?>" alt="Logo">
              </div>
              <div class="tabs-content">
                <h4>Selamat Datang</h4>
                <ul class="social-links">
                  <li><a href="#instansi">Daftar Instansi<i class="far fa-chevron-right"></i></a></li>
                  <li><a href="#fasilitas">Fasilitas<i class="far fa-chevron-right"></i></a></li>
                  <li><a href="#most-visited">Galeri<i class="far fa-chevron-right"></i></a></li>
                </ul>
              </div>
              <div class="page-direction-button">
                <a href="tel:<?=GetSetting('SETTING_ORG_PHONE')?>"><i class="far fa-phone-rotary"></i>&nbsp;CALL CENTER</a>
              </div>
          </div>
        </div>
        <div class="col-md-5 col-md-offset-1">
          <section id="first-tab-group" class="tabgroup">
            <div id="tab1">
              <div class="submit-form">
                <h4>Cek <em>Antrean</em>:</h4>
                <form id="form-submit" action="" method="get">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="instansi">Instansi:</label>
                        <select name="instansi" style="width: 100%" required>
                          <option value="">-- Pilih Instansi -- </option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="layanan">Layanan:</label>
                        <select name="layanan" style="width: 100%" required>
                          <option value="">-- Semua Layanan --</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <fieldset>
                        <button type="button" id="btn-cek" class="btn">CEK</button>
                      </fieldset>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </section>
  <section id="instansi">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
            <div class="section-heading">
                <h2>Daftar Instansi</h2>
            </div>
        </div>
        <div class="col-md-12">
          <div id="owl-instansi" class="owl-carousel owl-theme">
            <?php
            for($i=0; $i<=12; $i++) {
              ?>
              <div class="item col-md-12">
                <div class="visited-item">
                  <img src="<?=MY_IMAGEURL.'img-logo-mpp.png'?>" alt="">
                  <div class="text-content">
                    <h4>Instansi <?=str_pad($i+1,2,"0",STR_PAD_LEFT)?></h4>
                    <span>Lorem Ipsum</span>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="tabs-content" id="fasilitas">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-heading">
            <h2>Fasilitas</h2>
          </div>
        </div>
        <div class="wrapper">
          <div class="col-md-4">
            <ul class="tabs clearfix" data-tabgroup="third-tab-group">
              <li><a href="#ruangtunggu" class="active">Ruang Tunggu <i class="fa fa-angle-right"></i></a></li>
              <li><a href="#ruanglaktasi">Ruang Laktasi <i class="fa fa-angle-right"></i></a></li>
              <li><a href="#ruangbaca">Ruang Baca <i class="fa fa-angle-right"></i></a></li>
              <li><a href="#ruangbermain">Ruang Bermain Anak <i class="fa fa-angle-right"></i></a></li>
              <li><a href="#loketdifabel">Loket Disabilitas <i class="fa fa-angle-right"></i></a></li>
            </ul>
          </div>
          <div class="col-md-8">
            <section id="third-tab-group" class="recommendedgroup">
              <div id="ruangtunggu">
                <div class="row">
                  <div class="col-md-12">
                    <div class="owl-carousel owl-theme owl-facility">
                      <div class="item">
                        <div class="suiteroom-item">
                          <img src="<?=MY_IMAGEURL.'img-home-ruangtunggu01.jpeg'?>" alt="">
                          <div class="text-content">
                            <h4>Ruang Tunggu</h4>
                            <span>MPP Kota Tebing Tinggi</span>
                          </div>
                        </div>
                      </div>
                      <div class="item">
                        <div class="suiteroom-item">
                          <img src="<?=MY_IMAGEURL.'img-home-ruangtunggu02.jpeg'?>" alt="">
                          <div class="text-content">
                            <h4>Ruang Tunggu</h4>
                            <span>MPP Kota Tebing Tinggi</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="ruanglaktasi">
                <img src="<?=MY_IMAGEURL.'img-home-ruanglaktasi.jpeg'?>" alt="">
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-content">
                      <h4>Ruang Laktasi</h4>
                      <span>MPP Kota Tebing Tinggi</span>
                    </div>
                  </div>
                </div>
              </div>
              <div id="ruangbaca">
                <img src="<?=MY_IMAGEURL.'img-home-ruangbaca.jpeg'?>" alt="">
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-content">
                      <h4>Ruang Membaca</h4>
                      <span>MPP Kota Tebing Tinggi</span>
                    </div>
                  </div>
                </div>
              </div>
              <div id="ruangbermain">
                <img src="<?=MY_IMAGEURL.'img-home-ruangbermain.jpeg'?>" alt="">
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-content">
                      <h4>Ruang Bermain Anak</h4>
                      <span>MPP Kota Tebing Tinggi</span>
                    </div>
                  </div>
                </div>
              </div>
              <div id="loketdifabel">
                <img src="<?=MY_IMAGEURL.'img-home-loketdifabel.jpeg'?>" alt="">
                <div class="row">
                  <div class="col-md-12">
                    <div class="text-content">
                      <h4>Loket Khusus Disabilitas</h4>
                      <span>MPP Kota Tebing Tinggi</span>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section id="most-visited">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-heading"><h2>Galeri</h2></div>
        </div>
        <div class="col-md-12">
          <div id="owl-mostvisited" class="owl-carousel owl-theme">
            <?php
            for($i=0; $i<=12; $i++) {
              ?>
              <div class="item col-md-12">
                <div class="visited-item">
                  <img src="<?=MY_IMAGEURL.'img-logo-mpp.png'?>" alt="">
                  <div class="text-content">
                    <h4>Galeri <?=str_pad($i+1,2,"0",STR_PAD_LEFT)?></h4>
                    <span>Lorem Ipsum</span>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="primary-button"><a href="#" class="scroll-top">Kembali Ke Atas</a></div>
        </div>
        <div class="col-md-12">
          <ul class="social-icons">
            <li><a href="https://www.facebook.com/tooplate"><i class="fab fa-facebook"></i></a></li>
            <li><a href="#"><i class="fab fa-youtube"></i></a></li>
            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
            <li><a href="#"><i class="far fa-globe"></i></a></li>
          </ul>
        </div>
        <div class="col-md-12">
          <p>&copy; <?=date('Y')?> <?=GetSetting('SETTING_WEB_NAME')?> by: <a href="https://www.linkedin.com/in/yoelrolas/" target="_blank"><em>Partopi Tao</em></a></p>
        </div>
      </div>
    </div>
  </footer>
  <div class="modal" id="modal-antrean" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Daftar Antrean</h5>
        </div>
        <div class="modal-body" style="padding: 0 !important">
          <p>Modal body text goes here.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>
  <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?=base_url()?>assets/themes/flight/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>-->
  <script src="<?=base_url()?>assets/themes/flight/js/vendor/jquery-1.11.2.min.js"></script>

  <script src="<?=base_url()?>assets/themes/flight/js/datepicker.js"></script>
  <script src="<?=base_url()?>assets/themes/flight/js/plugins.js"></script>
  <script src="<?=base_url()?>assets/themes/flight/js/main.js"></script>

  <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>-->
  <script src="<?=base_url()?>assets/themes/gotto/js/jquery.min.js"></script>

  <script src="<?=base_url()?>assets/themes/flight/js/vendor/bootstrap.min.js"></script>

  <!-- Select 2 -->
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>

  <script src="<?=base_url()?>assets/themes/gotto/js/owl.carousel.min.js"></script>

  <script type="text/javascript">
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });
  $(document).ready(function() {
    $("select").not('.no-select2, .custom-select').select2({ width: 'resolve' });
    $("select[name=instansi]").load("https://antrianmpp.tebingtinggikota.go.id/api-mpp/get-opts-instansi.php", function(){
      $(this).trigger('change');
    });

    $("select[name=instansi]").change(function(){
      var inst = $(this).val();
      $("select[name=layanan]").load("https://antrianmpp.tebingtinggikota.go.id/api-mpp/get-opts-layanan.php?instansi="+inst, function(){

      });
    });

    $("#btn-cek").click(function(){
      var idinstansi = $('[name=instansi]').val();
      var idlayanan = $('[name=layanan]').val();
      var date = '<?=date('Y-m-d')?>';

      if(!idinstansi || !idlayanan) {
        alert('Harap pilih Instansi dan Layanan terlebih dahulu');
        return false;
      }

      $("#modal-antrean").modal('show');
      $(".modal-body", $("#modal-antrean")).load("https://antrianmpp.tebingtinggikota.go.id/api-mpp/get-html-antrian.php?instansi="+idinstansi+"&layanan="+idlayanan+"&date="+date, function(){

      });
    });

    $.get("https://antrianmpp.tebingtinggikota.go.id/api-mpp/get-json-instansi.php", function(data) {
  		var res = JSON.parse(data);
  		if(res.status=='OK') {
  			var html = '';
  			for(var i=0; i<res.data.length; i++) {
  				html += '<div class="item col-md-12">';
  				html += '<div class="visited-item">';
  				html += '<div style="background-image: url(\''+res.data[i].logo+'\'); background-size: cover; background-position: center;  width: 100%; height: 300px"></div>';
  				html += '<div class="text-content">';
  				html += '<h4>'+res.data[i].instansi+'</h4>';
  				html += '<span>'+(res.data[i].keterangan?res.data[i].keterangan:'-')+'</span>';
  				html += '</div>'
  				html += '</div>';
  				html += '</div>';
  			}
  			/*$("#owl-instansi").html(html);
  			$("#owl-instansi").owlCarousel({
  				pagination : true,
  				paginationNumbers: false,
  				autoPlay: 6000, //Set AutoPlay to 3 seconds
  				items : 4, //10 items above 1000px browser width
  				itemsDesktop : [1000,4], //5 items between 1000px and 901px
  				itemsDesktopSmall : [900,2], // betweem 900px and 601px
  				itemsTablet: [600,1], //2 items between 600 and 0
  				itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
  			});*/
  		}
  	});

    // navigation click actions
    $('.scroll-link').on('click', function(event){
        event.preventDefault();
        var sectionID = $(this).attr("data-id");
        scrollToID('#' + sectionID, 750);
    });
    // scroll to top action
    $('.scroll-top').on('click', function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop:0}, 'slow');
    });
    // mobile nav toggle
    $('#nav-toggle').on('click', function (event) {
        event.preventDefault();
        $('#main-nav').toggleClass("open");
    });
  });
  // scroll function
  function scrollToID(id, speed){
    var offSet = 0;
    var targetOffset = $(id).offset().top - offSet;
    var mainNav = $('#main-nav');
    $('html,body').animate({scrollTop:targetOffset}, speed);
    if (mainNav.hasClass("open")) {
      mainNav.css("height", "1px").removeClass("in").addClass("collapse");
      mainNav.removeClass("open");
    }
  }
  if (typeof console === "undefined") {
    console = {
        log: function() { }
    };
  }
  </script>
</body>
</html>
