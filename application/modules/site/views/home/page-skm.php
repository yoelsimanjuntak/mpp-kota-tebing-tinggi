<header class="site-header" style="background-image: url('<?=MY_IMAGEURL.'img-bg-overlay2.png'?>') !important">
  <div class="section-overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-12 text-center">
        <h2 class="text-white"><?=strtoupper($title)?></h2>
      </div>
    </div>
  </div>
</header>
<section class="reviews-section section-padding">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="reviews-thumb" style="padding: 20px !important">
          <iframe id="frameIKM" width="100%" height="720px" frameborder="0" scrolling="no" title="Indeks Kepuasan Masyarakat" referrerpolicy="origin" src="https://sikemas.tebingtinggikota.go.id/api-rekapitulasi-preview?filter[opd]=31&amp;filter[pelayanan]=&amp;filter[start]=<?=date('Y-01-01')?>&amp;filter[end]=<?=date('Y-m-d')?>"></iframe>
        </div>
      </div>
    </div>
  </div>
</section>
